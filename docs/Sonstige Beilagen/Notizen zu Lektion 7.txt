Notizen zur Lektion 7
---------------------

Laufzeitverhalten vom Merge Sort
--------------------------------

1. Anzahl Vergleiche insgesamt
2. Anzahl Kopiervorgänge insgesamt

-> kompliziert

Einfacher ist die Ermittlung des Laufzeitverhaltens
vom Mischen von zwei Arrays der Länge n

1. Anzahl Vergleiche < n
2. Anzahl Kopiervorgänge n

Aufwand für das Mischen von n Elementen O(n)





