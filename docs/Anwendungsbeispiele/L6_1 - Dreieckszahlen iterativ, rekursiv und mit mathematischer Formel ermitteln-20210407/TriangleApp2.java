// TriangleApp2.java
import java.io.*;
////////////////////////////////////////////////////////////////
class TriangleApp2
   {
   static int theNumber;

   public static void main(String[] args) throws IOException
      {
      System.out.print("Gib eine Zahl ein: ");
      theNumber = getInt();
      int theAnswer = triangle(theNumber);
      System.out.println("Dreieckszahl="+theAnswer);
      }  // end main()
//-------------------------------------------------------------
   public static int triangle(int n)
      {
      System.out.println("Aufruf: n=" + n);
      if(n==1)
         {
         System.out.println("R�ckgabe 1");
         return 1;
         }
      else
         {
         int temp = ( n + triangle(n-1) );
         System.out.println("R�ckgabe " + temp);
         return temp;
         }
      }
//-------------------------------------------------------------
   public static String getString() throws IOException
      {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
      }
//-------------------------------------------------------------
   public static int getInt() throws IOException
      {
      String s = getString();
      return Integer.parseInt(s);
      }
//--------------------------------------------------------------
   }  // end class TriangleApp2
////////////////////////////////////////////////////////////////
