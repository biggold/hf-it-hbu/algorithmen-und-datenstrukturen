// LinkListApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public int iData;              // Datenelement
   public double dData;           // Datenelement
   public Link next;              // Link
// -------------------------------------------------------------
   public Link(int id, double dd) // Konstruktor
      {
      iData = id;                 // Initialisiere die Datenelemente
      dData = dd;                 // ('next' wird automatisch auf 'null'
      }                           //  gesetzt)
// -------------------------------------------------------------
   public void displayLink()      // Zeige dich
      {
      System.out.print("{" + iData + ", " + dData + "} ");
      }
   }  // end class Link
////////////////////////////////////////////////////////////////
class LinkList
   {
   private Link first;            // Referenz auf den ersten Link

// -------------------------------------------------------------
   public LinkList()              // Konstruktor
      {
      first = null;               // Noch keine Links vorhanden
      }
// -------------------------------------------------------------
   public boolean isEmpty()       // True falls die Liste leer ist
      {
      return (first==null);
      }
// -------------------------------------------------------------
                                  // Einf�gen am Anfang
   public void insertFirst(int id, double dd)
      {                           // Erzeuge neues Link-Objekt
      Link newLink = new Link(id, dd);
      newLink.next = first;       // newLink --> altes first
      first = newLink;            // first --> newLink
      }
// -------------------------------------------------------------
   public Link deleteFirst()      // L�sche am Anfang
      {                           // (Annahme: Liste nicht leer)
      Link temp = first;          // Speichere Referenz
      first = first.next;         // L�sche: first-->altes next
      return temp;                // Gib gel�schten Link zur�ck
      }
// -------------------------------------------------------------
   public void displayList()
      {
      System.out.print("Liste (Anfang-->Ende): ");
      Link current = first;       // Starte am Anfang
      while(current != null)      // Solange bis das Ende erreicht wird,
         {
         current.displayLink();   // Zeige Datenelemente
         current = current.next;  // Bewege zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   }  // end class LinkList
////////////////////////////////////////////////////////////////
class LinkListApp
   {
   public static void main(String[] args)
      {
      LinkList theList = new LinkList();  // Erzeuge neue Liste

      theList.insertFirst(22, 2.99);      // F�ge 4 Datens�tze ein
      theList.insertFirst(44, 4.99);
      theList.insertFirst(66, 6.99);
      theList.insertFirst(88, 8.99);

      theList.displayList();              // Zeige die Liste

      while( !theList.isEmpty() )         // So lange Liste nicht leer,
         {
         Link aLink = theList.deleteFirst();   // L�sche Link-Objekt
         System.out.print("Gel�scht ");        // Zeige es
         aLink.displayLink();
         System.out.println("");
         }
      theList.displayList();              // Zeige Liste
      }  // end main()
   }  // end class LinkListApp
////////////////////////////////////////////////////////////////
