// LinkListApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public int iData;              // Datenelement
   public double dData;           // Datenelement
   public Link next;              // Link
// -------------------------------------------------------------
   public Link(int id, double dd) // Konstruktor
      {
      iData = id;                 // Initialisiere die Datenelemente
      dData = dd;                 // ('next' wird automatisch auf 'null'
      }                           //  gesetzt)
// -------------------------------------------------------------
   public void displayLink()      // Zeige dich
      {
      System.out.print("{" + iData + ", " + dData + "} ");
      }
   }  // end class Link
////////////////////////////////////////////////////////////////
class LinkList
   {
   private Link first;            // Referenz auf den ersten Link

// -------------------------------------------------------------
   public LinkList()              // Konstruktor
      {
      first = null;               // Noch keine Links vorhanden
      }
// -------------------------------------------------------------
   public boolean isEmpty()       // True falls die Liste leer ist
      {
      return true;
      }
// -------------------------------------------------------------
                                  // Einf�gen am Anfang
   public void insertFirst(int id, double dd)
      {
      }
// -------------------------------------------------------------
   public Link deleteFirst()      // L�sche am Anfang
      {
      return null;                // Gib gel�schten Link zur�ck
      }
// -------------------------------------------------------------
   public void displayList()
      {
      }
// -------------------------------------------------------------
   }  // end class LinkList
////////////////////////////////////////////////////////////////
class LinkListApp
   {
   public static void main(String[] args)
      {
      LinkList theList = new LinkList();  // Erzeuge neue Liste

      theList.insertFirst(22, 2.99);      // F�ge 4 Datens�tze ein
      theList.insertFirst(44, 4.99);
      theList.insertFirst(66, 6.99);
      theList.insertFirst(88, 8.99);

      theList.displayList();              // Zeige die Liste

      while( !theList.isEmpty() )         // So lange Liste nicht leer,
         {
         Link aLink = theList.deleteFirst();   // L�sche Link-Objekt
         System.out.print("Gel�scht ");        // Zeige es
         aLink.displayLink();
         System.out.println("");
         }
      theList.displayList();              // Zeige Liste
      }  // end main()
   }  // end class LinkListApp
////////////////////////////////////////////////////////////////
