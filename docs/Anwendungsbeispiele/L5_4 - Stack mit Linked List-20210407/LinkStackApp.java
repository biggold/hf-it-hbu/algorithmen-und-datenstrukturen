// LinkStackApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public long dData;             // Datensatz
   public Link next;              // Referenz auf das n�chste Link-Objekt
// -------------------------------------------------------------
   public Link(long dd)           // Konstruktor
      { dData = dd; }
// -------------------------------------------------------------
   public void displayLink()      // Zeige dich
      { System.out.print(dData + " "); }
   }  // end class Link
////////////////////////////////////////////////////////////////
class LinkList
   {
   private Link first;            // Referenz auf erstes Link-Objekt
// -------------------------------------------------------------
   public LinkList()              // Konstruktor
      { first = null; }           // Noch keine Datens�tze in der Liste
// -------------------------------------------------------------
   public boolean isEmpty()       // True falls Liste leer
      { return (first==null); }
// -------------------------------------------------------------
   public void insertFirst(long dd) // Einf�gen am Anfang
      {                           // Erzeuge neues Link-Objekt
      Link newLink = new Link(dd);
      newLink.next = first;       // newLink --> altes 'first'
      first = newLink;            // 'first' --> newLink
      }
// -------------------------------------------------------------
   public long deleteFirst()      // L�sche erstes Element
      {                           // (Annahme: Liste nicht leer)
      Link temp = first;          // Speichere die Referenz zum Link-Objekt
      first = first.next;         // L�sche es: 'first' --> altes next
      return temp.dData;          // Gib Daten des gel�schten Link-Objekts zur�ck
      }
// -------------------------------------------------------------
   public void displayList()
      {
      Link current = first;       // Starte am Anfang
      while(current != null)      // Solage das Ende der Liste nicht erreicht ist,
         {
         current.displayLink();   // Zeige Daten
         current = current.next;  // Bewege zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   }  // end class LinkList
////////////////////////////////////////////////////////////////
class LinkStack
   {
   private LinkList theList;
//--------------------------------------------------------------
   public LinkStack()             // Konstruktor
      {
      theList = new LinkList();
      }
//--------------------------------------------------------------
   public void push(long j)     // Push Element auf Stack
      {
      theList.insertFirst(j);
      }
//--------------------------------------------------------------
   public long pop()            // Pop Element vom Stack
      {
      return theList.deleteFirst();
      }
//--------------------------------------------------------------
   public boolean isEmpty()       // True falls Stack leer
      {
      return ( theList.isEmpty() );
      }
//--------------------------------------------------------------
   public void displayStack()
      {
      System.out.print("Stack (Oben-->Unten): ");
      theList.displayList();
      }
//--------------------------------------------------------------
   }  // end class LinkStack
////////////////////////////////////////////////////////////////
class LinkStackApp
   {
   public static void main(String[] args)
      {
      LinkStack theStack = new LinkStack(); // Erzeuge Stack

      theStack.push(20);                    // Push
      theStack.push(40);

      theStack.displayStack();              // Zeige Stack-Inhalt

      theStack.push(60);                    // Push
      theStack.push(80);

      theStack.displayStack();              // Zeige Stack-Inhalt

      theStack.pop();                       // Pop
      theStack.pop();

      theStack.displayStack();              // Zeige Stack-Inhalt
      }  // end main()
   }  // end class LinkStackApp
////////////////////////////////////////////////////////////////
