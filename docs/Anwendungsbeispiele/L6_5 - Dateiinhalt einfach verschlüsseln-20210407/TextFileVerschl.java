// TextFileVerschl.java
import java.io.*;
////////////////////////////////////////////////////////////////
public class TextFileVerschl {

  public static String verschl(String s,    // Verschlüssle String s
                               byte ivKey) {// Startschlüssel zur Verschlüsselung
                                            // nach CBC (Cipher Block Chaining Mode)
                                            // mit C0 = E(P0 xor IV)
                                            // und Ci = E(Pi xor Ci-1)
                                            // E ist Addition von 1
                                            
    int         i;                          // Schleifenzähler
    String      res;                        // Resultatstring
    char        ch;                         // Einzelnes Zeichen aus String s
    byte        charCode;                   // ASCII-Code des Zeichens
    byte        key;                        // Aktueller Schlüssel
    
    res = "";                               // Resultatstring am Anfang leer
    key = ivKey;
    for (i = 0; i < s.length() ; i++){      // Für jedes Zeichen aus String s
      ch = s.charAt(i);                     // Speichere es in ch
      charCode = (byte) ch;                 // Ermittle seinen ASCII-Code
      
      charCode = (byte) ((charCode^key)+1); // Verschlüsselung
      key =charCode;
      
      ch = (char) charCode;                 // Erzeuge neues Zeichen mit neuem Code
      res = res + ch;                       // Hänge neues Zeichen an Resultatsting an
    }
    return res;                             // Gib Resultatstring zurück
  }

  public static String entschl(String s,    // Entschlüssle String s
                               byte ivKey) {// Startschlüssel zur Entschlüsselung
                                            // nach CBC (Cipher Block Chaining Mode)
                                            // mit P0 = D(C0) xor IV
                                            // und Pi = D(Ci) xor Ci-1
                                            // D ist Sutraktion von 1
                                            
    int         i;                          // Schleifenzähler
    String      res;                        // Resultatstring
    char        ch;                         // Einzelnes Zeichen aus String s
    byte        charCode;                   // ASCII-Code des Zeichens
    byte        charCodeOld;
    byte        key;

    res = "";                               // Resultatstring am Anfang leer
    key = ivKey;
    for (i = 0; i < s.length() ; i++){      // Für jedes Zeichen aus String s
      ch = s.charAt(i);                     // Speichere es in ch
      charCode = (byte) ch;                 // Ermittle seinen ASCII-Code
      
      charCodeOld = charCode;               // Entschlüsselung
      charCode = (byte) ((charCode-1)^key);
      key =charCodeOld;
      
      ch = (char) charCode;                 // Gewinne ursprüngliches Zeichen zurück
      res = res + ch;                       // Hänge das Zeichen an Resultatsting an
    }
    return res;                             // Gib Resultatstring zurück
  }
  
  public static void main(String[] args) throws IOException {

    String           inFileName = "C:\\Temp\\Test.txt";  // Festlegen Dateinamen Input-Datei, Annahme: Datei vorhanden
    File             inFile = new File(inFileName);      // 1. Datei-Objekt erzeugen (Input)
    BufferedReader   tfin = new BufferedReader(new FileReader(inFileName)); // Input-Datei lesend öffnen
    
    String           outFileName = "C:\\Temp\\TestVerschl.txt"; // Festlegen Dateinamen Output-Datei
    File             outFile = new File(outFileName);           // 2. Datei-Objekt erzeugen (Output)
    BufferedWriter   tfout = new BufferedWriter(new FileWriter(outFileName)); // Output-Datei schreibend öffnen
    
    String           line;
    byte             ivKey = (byte) 11;              // Ver- und Entschlüsselungsschlüssel zwischen 0 und 255
    
    while ((line=tfin.readLine()) != null) {         // Solange das Dateiende der Input-Datei nicht erreicht ist,
                                                     // Lese Zeile aus Input-Datei
      System.out.println("Original ------> " + line);// Gib zur Kontrolle Zeile auf Bildschirm aus
      line = verschl(line, ivKey);                   // Verschlüssle Zeile
      System.out.println("Verschlüsselt -> " + line);// Gib zur Kontrolle verschlüsselte Zeile auf Bildschirm aus
      tfout.write(line);                             // Schreibe verschlüsselte Zeile in Output-Datei
      tfout.newLine();                               // Zeilenumbruch in Output-Datai
      line = entschl(line, ivKey);                   // Entschlüssle Zeile
      System.out.println("Entschlüsselt -> " + line);// Gib zur Kontrolle die zurückentschlüsselte Zeile aus
      System.out.println();
    }
    tfin.close();                                    // Schliesse Input-Datei
    tfout.close();                                   // Schliesse Output-Datei
  }
  
}
////////////////////////////////////////////////////////////////