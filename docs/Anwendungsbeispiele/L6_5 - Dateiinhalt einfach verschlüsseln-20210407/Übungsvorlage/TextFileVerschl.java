// TextFileVerschl.java
import java.io.*;
////////////////////////////////////////////////////////////////
public class TextFileVerschl {

  public static String verschl(String s) {  // Verschl�ssle String s

    int         i;                          // Schleifenz�hler
    String      res;                        // Resultatstring
    char        ch;                         // Einzelnes Zeichen aus String s
    int         charCode;                   // ASCII-Code des Zeichens
    
    res = "";                               // Resultatstring am Anfang leer
    for (i = 0; i < s.length() ; i++){      // F�r jedes Zeichen aus String s
      ch = s.charAt(i);                     // Speichere es in ch
      charCode = (byte) ch;                 // Ermittle seinen ASCII-Code
      
                                            // Erfinde neuen Code (Verschl�sselung)
      
      ch = (char) charCode;                 // Erzeuge neues Zeichen mit neuem Code
      res = res + ch;                       // H�nge neues Zeichen an Resultatsting an
    }
    return res;                             // Gib Resultatstring zur�ck
  }

  public static String entschl(String s) {  // Entschl�ssle String s
  
    int         i;                          // Schleifenz�hler
    String      res;                        // Resultatstring
    char        ch;                         // Einzelnes Zeichen aus String s
    int         charCode;                   // ASCII-Code des Zeichens

    res = "";                               // Resultatstring am Anfang leer
    for (i = 0; i < s.length() ; i++){      // F�r jedes Zeichen aus String s
      ch = s.charAt(i);                     // Speichere es in ch
      charCode = (byte) ch;                 // Ermittle seinen ASCII-Code
      
                                            // Entschl�ssle Code
      
      ch = (char) charCode;                 // Gewinne urspr�ngliches Zeichen zur�ck
      res = res + ch;                       // H�nge das Zeichen an Resultatsting an
    }
    return res;                             // Gib Resultatstring zur�ck
  }
  
  public static void main(String[] args) throws IOException {

    String           inFileName = "C:\\Temp\\Test.txt";  // Festlegen Dateinamen Input-Datei, Annahme: Datei vorhanden
    File             inFile = new File(inFileName);      // 1. Datei-Objekt erzeugen (Input)
    BufferedReader   tfin = new BufferedReader(new FileReader(inFileName)); // Input-Datei lesend �ffnen
    
    String           outFileName = "C:\\Temp\\TestVerschl.txt"; // Festlegen Dateinamen Output-Datei
    File             outFile = new File(outFileName);           // 2. Datei-Objekt erzeugen (Output)
    BufferedWriter   tfout = new BufferedWriter(new FileWriter(outFileName)); // Output-Datei schreibend �ffnen
    
    String           line;
    
    while ((line=tfin.readLine()) != null) {         // Solange das Dateiende der Input-Datei nicht erreicht ist,
                                                     // Lese Zeile aus Input-Datei
      System.out.println("Original ------> " + line);// Gib zur Kontrolle Zeile auf Bildschirm aus
      line = verschl(line);                          // Verschl�ssle Zeile
      System.out.println("Verschl�sselt -> " + line);// Gib zur Kontrolle verschl�sselte Zeile auf Bildschirm aus
      tfout.write(line);                             // Schreibe verschl�sselte Zeile in Output-Datei
      tfout.newLine();                               // Zeilenumbruch in Output-Datai
      line = entschl(line);                          // Entschl�ssle Zeile
      System.out.println("Entschl�sselt -> " + line);// Gib zur Kontrolle die zur�ckentschl�sselte Zeile aus
      System.out.println();
    }
    tfin.close();                                    // Schliesse Input-Datei
    tfout.close();                                   // Schliesse Output-Datei
  }
  
}
////////////////////////////////////////////////////////////////