// Queue.java
////////////////////////////////////////////////////////////////
class Queue
   {
   private int maxSize;
   private long[] queArray;
   private int front;
   private int rear;
   private int nItems;
//--------------------------------------------------------------
   public Queue(int s)          // Konstruktor
      {
      maxSize = s;
      queArray = new long[maxSize];
      front = 0;
      rear = -1;
      nItems = 0;
      }
//--------------------------------------------------------------
   public void insert(long j)   // Datensatz am Ende einf�gen
      {

      }
//--------------------------------------------------------------
   public long remove()         // Datensatz am Anfang ergreifen
      {
      return 0;
      }
//--------------------------------------------------------------
   public long peekFront()      // Datensatz am Anfang der Queue
      {
      return 0;
      }
//--------------------------------------------------------------
   public boolean isEmpty()    // True falls Queue leer
      {
      return true;
      }
//--------------------------------------------------------------
   public boolean isFull()     // True falls Queue voll
      {
      return true;
      }
//--------------------------------------------------------------
   public int size()           // Anzahl Datens�tze in der Queue
      {
      return 0;
      }
//--------------------------------------------------------------
   }  // end class Queue
////////////////////////////////////////////////////////////////
class QueueApp
   {
   public static void main(String[] args)
      {
      Queue theQueue = new Queue(5);  // Queue mit 5 Speicherpl�tzen

      theQueue.insert(10);            // F�ge 4 Datens�tze ein
      theQueue.insert(20);
      theQueue.insert(30);
      theQueue.insert(40);

      theQueue.remove();              // Entferne 3 Datens�tze
      theQueue.remove();              //    (10, 20, 30)
      theQueue.remove();

      theQueue.insert(50);            // F�ge 4 weitere ein
      theQueue.insert(60);            //    (Umbruch n�tig)
      theQueue.insert(70);
      theQueue.insert(80);

      while( !theQueue.isEmpty() )    // Entferne alle und zeige sie
         {                            //
         long n = theQueue.remove();  // (40, 50, 60, 70, 80)
         System.out.print(n);
         System.out.print(" ");
         }
      System.out.println("");
      }  // end main()
   }  // end class QueueApp
////////////////////////////////////////////////////////////////
