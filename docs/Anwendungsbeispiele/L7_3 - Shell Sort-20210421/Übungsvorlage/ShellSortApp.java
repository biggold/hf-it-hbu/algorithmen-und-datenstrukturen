// ShellSortApp.java
//--------------------------------------------------------------
class ArraySh
   {
   private long[] theArray;          // Referenz auf theArray
   private int nElems;               // Anzahl Datens�tze
//--------------------------------------------------------------
   public ArraySh(int max)           // Konsztruktor
      {
      theArray = new long[max];      // Erzeuge Array
      nElems = 0;                    // Noch keine Datens�tze
      }
//--------------------------------------------------------------
   public void insert(long value)    // Datensatz in Array einf�gen
      {
      theArray[nElems] = value;      // F�ge ein
      nElems++;                      // Inkrementiere Array-Gr�sse
      }
//--------------------------------------------------------------
   public void display()             // Zeige Array-Inhalt
      {
      System.out.print("A=");
      for(int j=0; j<nElems; j++)    // F�r jeden Datensatz,
         System.out.print(theArray[j] + " ");  // Zeige es
      System.out.println("");
      }
//--------------------------------------------------------------
   public void shellSort()
      {
      int inner, outer;
      long temp;

      int h = 1;                     // Finde Startwert f�r h

         h = h;                      // (1, 4, 13, 40, 121, ...)

      while(h>0)                     // Dekrementiere h, bis h=1
         {
                                     // h-Sort die Elemente
         for(outer=h; outer<nElems; outer++)
            {
            temp = theArray[outer];
            inner = outer;
                                     // Ein Sub-Lauf (d.h. 0, 4, 8)
            while(inner > h-1 && theArray[inner-h] >=  temp)
               {
               theArray[inner] = theArray[inner-h];
               inner -= h;
               }
            theArray[inner] = temp;
            }  // end for
         h = h-1;              // Dekrementiere h
         }  // end while(h>0)
      }  // end shellSort()
//--------------------------------------------------------------
   }  // end class ArraySh
////////////////////////////////////////////////////////////////
class ShellSortApp
   {
   public static void main(String[] args)
      {
      int maxSize = 10;             // Array-Gr�sse
      ArraySh arr;
      arr = new ArraySh(maxSize);   // Erzeuge Array

      for(int j=0; j<maxSize; j++)  // F�lle Array mit
         {                          // Zufallszahlen
         long n = (int)(java.lang.Math.random()*99);
         arr.insert(n);
         }
      arr.display();                // Zeige unsortiertes Array
      arr.shellSort();              // Shell Sort
      arr.display();                // Zeige sortiertes Array
      }  // end main()
   }  // end class ShellSortApp
////////////////////////////////////////////////////////////////
