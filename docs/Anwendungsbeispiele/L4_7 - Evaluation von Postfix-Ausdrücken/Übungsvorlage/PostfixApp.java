// PostfixApp.java
import java.io.*;              // for I/O
////////////////////////////////////////////////////////////////
class StackX
   {
   private int maxSize;
   private int[] stackArray;
   private int top;
//--------------------------------------------------------------
   public StackX(int size)      // Konstruktor
      {
      maxSize = size;
      stackArray = new int[maxSize];
      top = -1;
      }
//--------------------------------------------------------------
   public void push(int j)     // Element in Stack einf�gen
      { stackArray[++top] = j; }
//--------------------------------------------------------------
   public int pop()            // Element vom Stack wegnehmen
      { return stackArray[top--]; }
//--------------------------------------------------------------
   public int peek()           // Aktuelles Element im Stack
      { return stackArray[top]; }
//--------------------------------------------------------------
   public boolean isEmpty()    // True falls Stack leer
      { return (top == -1); }
//--------------------------------------------------------------
   public boolean isFull()     // True falls Stack voll
      { return (top == maxSize-1); }
//--------------------------------------------------------------
   public int size()           // Anzahl Elemente im Stack
      { return top+1; }
//--------------------------------------------------------------
   public int peekN(int n)     // Element an Stelle n
      { return stackArray[n]; }
//--------------------------------------------------------------
   public void displayStack(String s)
      {
      System.out.print(s);
      System.out.print("Stack (unten-->oben): ");
      for(int j=0; j<size(); j++)
         {
         System.out.print( peekN(j) );
         System.out.print(' ');
         }
      System.out.println("");
      }
//--------------------------------------------------------------
   }  // end class StackX
////////////////////////////////////////////////////////////////
class ParsePost
   {
   private StackX theStack;
   private String input;
//--------------------------------------------------------------
   public ParsePost(String s)
      { input = s; }
//--------------------------------------------------------------
   public int doParse()
      {
      return 0;
      }  // end doParse()
   }  // end class ParsePost
////////////////////////////////////////////////////////////////
class PostfixApp
   {
   public static void main(String[] args) throws IOException
      {
      String input;
      int output;

      while(true)
         {
         System.out.print("Postfix-Ausdruck: ");
         System.out.flush();
         input = getString();         // Lese String von Tastatur
         if( input.equals("") )       // Ende falls [Enter]
            break;
                                      // Erzeuge Parser-Objekt
         ParsePost aParser = new ParsePost(input);
         output = aParser.doParse();  // Evaluiere
         System.out.println("Resultat " + output);
         }  // end while
      }  // end main()
//--------------------------------------------------------------
   public static String getString() throws IOException
      {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
      }
//--------------------------------------------------------------
   }  // end class PostfixApp
////////////////////////////////////////////////////////////////
