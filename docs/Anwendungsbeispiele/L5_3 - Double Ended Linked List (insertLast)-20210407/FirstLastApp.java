// FirstLastApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public long dData;                 // Datenelement
   public Link next;                  // Link auf n�chstes Link-Objekt
// -------------------------------------------------------------
   public Link(long d)                // Konstruktor
      { dData = d; }
// -------------------------------------------------------------
   public void displayLink()          // Zeige Datenelement
      { System.out.print(dData + " "); }
// -------------------------------------------------------------
   }  // end class Link
////////////////////////////////////////////////////////////////
class FirstLastList
   {
   private Link first;               // Referenz auf 'first'-Link-Objekt
   private Link last;                // Referenz auf 'last'-Link-Objekt
// -------------------------------------------------------------
   public FirstLastList()            // Konstruktor
      {
      first = null;                  // Noch keine Referenzen
      last = null;
      }
// -------------------------------------------------------------
   public boolean isEmpty()          // True falls keine Link-Objekte vorhanden
      { return first==null; }
// -------------------------------------------------------------
   public void insertFirst(long dd)  // Einf�gen am Anfang
      {
      Link newLink = new Link(dd);   // Erzeuge neues Link-Objekt

      if( isEmpty() )                // Falls Liste leer,
         last = newLink;             // newLink <-- 'last'
      newLink.next = first;          // newLink --> altes 'first'
      first = newLink;               // 'first' --> newLink
      }
// -------------------------------------------------------------
   public void insertLast(long dd)   // Einf�gen am Ende
      {
      Link newLink = new Link(dd);   // Erzeuge neues Link-Objekt
      if( isEmpty() )                // Falls Liste leer,
         first = newLink;            // 'first' --> newLink
      else
         last.next = newLink;        // altes 'last' --> newLink
      last = newLink;                // newLink <-- 'last'
      }
// -------------------------------------------------------------
   public long deleteFirst()         // L�sche erstes Link-Objekt
      {                              // (Annahme: Liste nicht leer)
      long temp = first.dData;
      if(first.next == null)         // Fall nur ein Link-Objekt
         last = null;                // null <-- 'last'
      first = first.next;            // 'first' --> altes next
      return temp;
      }
// -------------------------------------------------------------
   public void displayList()
      {
      System.out.print("Liste (Anfang-->Ende): ");
      Link current = first;          // Starte beim Anfang
      while(current != null)         // Bis das Ende erreicht wird,
         {
         current.displayLink();      // Gib Daten aus
         current = current.next;     // Bewege zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   }  // end class FirstLastList
////////////////////////////////////////////////////////////////
class FirstLastApp
   {
   public static void main(String[] args)
      {                              // Erzeuge eine neue Liste
      FirstLastList theList = new FirstLastList();

      theList.insertFirst(22);       // Einf�gen am Anfang
      theList.insertFirst(44);
      theList.insertFirst(66);

      theList.insertLast(11);        // Einf�gen am Ende
      theList.insertLast(33);
      theList.insertLast(55);

      theList.displayList();         // Zeige den Inhalt der Liste

      theList.deleteFirst();         // L�sche die ersten zwei Link-Objekte
      theList.deleteFirst();

      theList.displayList();         // Zeige erneut
      }  // end main()
   }  // end class FirstLastApp
////////////////////////////////////////////////////////////////
