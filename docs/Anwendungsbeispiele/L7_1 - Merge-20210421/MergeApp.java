// MergeApp.java
////////////////////////////////////////////////////////////////
class MergeApp
   {
   public static void main(String[] args)
      {
      int[] arrayA = {23, 47, 81, 95};
      int[] arrayB = {7, 14, 39, 55, 62, 74};
      int[] arrayC = new int[10];

      merge(arrayA, 4, arrayB, 6, arrayC);
      display(arrayC, 10);
      }  // end main()
   //-----------------------------------------------------------
                                       // Mische A und B in C
     public static void merge( int[] arrayA, int sizeA,
                               int[] arrayB, int sizeB,
                               int[] arrayC )
      {
      int aDex=0, bDex=0, cDex=0;

      while(aDex < sizeA && bDex < sizeB)  // A und B nicht leer
         if( arrayA[aDex] < arrayB[bDex] )
            arrayC[cDex++] = arrayA[aDex++];
         else
            arrayC[cDex++] = arrayB[bDex++];

      while(aDex < sizeA)                  // Array B ist leer,
         arrayC[cDex++] = arrayA[aDex++];  // Aber Array A nicht
         
      while(bDex < sizeB)                  // Array A ist leer,
         arrayC[cDex++] = arrayB[bDex++];  // Aber Array B nicht
      }  // end MergeApp()
   //-----------------------------------------------------------
                                       // Zeige Array
   public static void display(int[] theArray, int size)
      {
      for(int j=0; j<size; j++)
         System.out.print(theArray[j] + " ");
      System.out.println("");
      }
   //-----------------------------------------------------------
   }  // end class MergeApp
////////////////////////////////////////////////////////////////
