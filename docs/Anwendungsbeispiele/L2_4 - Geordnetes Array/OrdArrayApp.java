// OrdArrayApp.java
////////////////////////////////////////////////////////////////
class OrdArray
   {
   private long[] a;                 // Array-Referenz
   private int nElems;               // Anzahl Elemente
   //-----------------------------------------------------------
   public OrdArray(int max)          // Konstruktor
      {
      a = new long[max];             // Erzeuge Array
      nElems = 0;
      }
   //-----------------------------------------------------------
   public int size()
      { return nElems; }
   //-----------------------------------------------------------
   public int find(long searchKey)
      {
      int lowerBound = 0;
      int upperBound = nElems-1;
      int curIn;

      while(true)
         {
         curIn = (lowerBound + upperBound ) / 2;
         if(a[curIn]==searchKey)
            return curIn;              // Gefunden
         else if(lowerBound > upperBound)
            return nElems;             // Kann es nicht finden
         else                          // Halbiere Bereich
            {
            if(a[curIn] < searchKey)
               lowerBound = curIn + 1; // Es ist im oberen Bereich
            else
               upperBound = curIn - 1; // Es ist im unteren Bereich
            }  // end else divide range
         }  // end while
      }  // end find()
   //-----------------------------------------------------------
   public void insert(long value)    // Element einf�gen
      {
      int j;
      for(j=0; j<nElems; j++)        // Finde wo es hingeh�rt
         if(a[j] > value)            // (Lineare Suche)
            break;
      for(int k=nElems; k>j; k--)    // Versetze die gr�sseren nach oben
         a[k] = a[k-1];
      a[j] = value;                  // F�ge es ein
      nElems++;                      // Inkrementiere die Anzahl
      }  // end insert()
   //-----------------------------------------------------------
   public boolean delete(long value)
      {
      int j = find(value);
      if(j==nElems)                  // Kann es nicht finden
         return false;
      else                           // Gefunden
         {
         for(int k=j; k<nElems; k++) // Versetze die gr�sseren nach unten
            a[k] = a[k+1];
         nElems--;                   // Dekrementiere die Anzahl
         return true;
         }
      }  // end delete()
   //-----------------------------------------------------------
   public void display()             // Zeige Array-Inhalt
      {
      for(int j=0; j<nElems; j++)       // F�r jedes Element,
         System.out.print(a[j] + " ");  // Zeige es
      System.out.println("");
      }
   //-----------------------------------------------------------
   }  // end class OrdArray
////////////////////////////////////////////////////////////////
class OrdArrayApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;             // Array-Gr�sse
      OrdArray arr;                  // Array-Referenz
      arr = new OrdArray(maxSize);   // Erzeuge Array

      arr.insert(77);                // F�ge 10 Elemente ein
      arr.insert(99);
      arr.insert(44);
      arr.insert(55);
      arr.insert(22);
      arr.insert(88);
      arr.insert(11);
      arr.insert(00);
      arr.insert(66);
      arr.insert(33);

      int searchKey = 55;            // Suche Element 55
      if( arr.find(searchKey) != arr.size() )
         System.out.println("Gefunden " + searchKey);
      else
         System.out.println("Finde nicht " + searchKey);

      arr.display();                 // Zeige Array-Inhalt

      arr.delete(00);                // L�sche drei Elemente (00, 55, 99)
      arr.delete(55);
      arr.delete(99);

      arr.display();                 // Zeige Array-Inhalt erneut
      }  // end main()
   }  // end class OrdArrayApp
