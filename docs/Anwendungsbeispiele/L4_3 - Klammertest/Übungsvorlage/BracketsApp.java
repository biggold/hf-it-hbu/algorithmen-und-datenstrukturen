// BracketsApp.java
import java.io.*;                 // for I/O
////////////////////////////////////////////////////////////////
class StackX
   {
   private int maxSize;
   private char[] stackArray;
   private int top;
//--------------------------------------------------------------
   public StackX(int s)       // Konstruktor
      {
      maxSize = s;
      stackArray = new char[maxSize];
      top = -1;
      }
//--------------------------------------------------------------
   public void push(char j)  // Datensatz in Stack einf�gen
      {
      stackArray[++top] = j;
      }
//--------------------------------------------------------------
   public char pop()         // Datensatz von Stack entfernen
      {
      return stackArray[top--];
      }
//--------------------------------------------------------------
   public char peek()        // Aktueller Datensatz lesen
      {
      return stackArray[top];
      }
//--------------------------------------------------------------
   public boolean isEmpty()    // True falls Stack leer
      {
      return (top == -1);
      }
//--------------------------------------------------------------
   }  // end class StackX
////////////////////////////////////////////////////////////////
class BracketChecker
   {
   private String input;                   // Input String
//--------------------------------------------------------------
   public BracketChecker(String in)        // Konstruktor
      { input = in; }
//--------------------------------------------------------------
   public void check()
      {
      int stackSize = input.length();      // Bestimme Stack-Gr�sse
      StackX theStack = new StackX(stackSize);  // Erzeuge Stack

      for(int j=0; j<input.length(); j++)  // Extrahiere die einzelnen Chars
         {
         char ch = input.charAt(j);        // Hole Char
         switch(ch)
            {

            default:    // mache nichts bei anderen chars
               break;
            }  // end switch
         }  // end for
      // An dieser Stelle sind alle Zeichen (chars) �berpr�ft
      if( !theStack.isEmpty() )
         System.out.println("Fehler: Rechte Klammer fehlt");
      }  // end check()
//--------------------------------------------------------------
   }  // end class BracketChecker
////////////////////////////////////////////////////////////////
class BracketsApp
   {
   public static void main(String[] args) throws IOException
      {
      String input;
      while(true)
         {
         System.out.print(
                      "String mit Klammern eigeben: ");
         System.out.flush();
         input = getString();     // Lese String von der Tastatur
         if( input.equals("") )   // Ende falls [Enter]
            break;
                                  // Erzeuge BracketChecker-Objekt
         BracketChecker theChecker = new BracketChecker(input);
         theChecker.check();      // Teste die Klammerung
         }  // end while
      }  // end main()
//--------------------------------------------------------------
   public static String getString() throws IOException
      {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
      }
//--------------------------------------------------------------
   }  // end class BracketsApp
////////////////////////////////////////////////////////////////
