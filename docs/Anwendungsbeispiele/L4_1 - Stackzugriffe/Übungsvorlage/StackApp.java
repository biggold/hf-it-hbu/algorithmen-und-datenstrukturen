// StackApp.java
////////////////////////////////////////////////////////////////
class StackX
   {
   private int maxSize;        // Gr�sse des Stack (Array)
   private long[] stackArray;
   private int top;            // Top des Stack
//--------------------------------------------------------------
   public StackX(int s)         // Konstruktor
      {
      maxSize = s;             // Setze die Gr�sse
      stackArray = new long[maxSize];  // Erzeuge Array
      top = -1;                // Noch keine Datens�tze
      }
//--------------------------------------------------------------
   public void push(long j)    // Datensatz einf�gen
      {
                               // Inkrementiere Top, f�ge Datensatz ein
      }
//--------------------------------------------------------------
   public long pop()           // Datensatz entfernen
      {
      return 0;                // Ergreife Datensatz, dekrementiere Top
      }
//--------------------------------------------------------------
   public long peek()          // Aktueller Datensatz
      {
      return 0;
      }
//--------------------------------------------------------------
   public boolean isEmpty()    // True falls Stack leer
      {
      return true;
      }
//--------------------------------------------------------------
   public boolean isFull()     // True falls Stack voll
      {
      return false;
      }
//--------------------------------------------------------------
   }  // end class StackX
////////////////////////////////////////////////////////////////
class StackApp
   {
   public static void main(String[] args)
      {
      StackX theStack = new StackX(10);  // Erzeuge neuen Stack
      theStack.push(20);               // F�ge Datens�tze ein
      theStack.push(40);
      theStack.push(60);
      theStack.push(80);

      while( !theStack.isEmpty() )     // Solange Stack nicht leer,
         {                             // Entferne Datensatz
         long value = theStack.pop();
         System.out.print(value);      // Zeige es
         System.out.print(" ");
         }  // end while
      System.out.println("");
      }  // end main()
   }  // end class StackApp
////////////////////////////////////////////////////////////////
