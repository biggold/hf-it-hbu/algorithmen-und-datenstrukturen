/*
  Ermitteln des ggT(a,b) nach Euklid
  
  Version 1.0
  
  08.02.2013
  
  Autor: G. Groppo
*/
public class Euklid {                       // Klassendeklaration
  public static void main(String[] args) {  // Deklaration der main()-Methode

    int a, b;                               // Deklaration von zwei ganzahligen Variablen a und b (Speicheradressen)
    int resultat;                           // Deklaration einer weiteren Variablen zur Speicherung des ggT(a,b)
    int a_orig, b_orig;

    a = 1029;                                 // Wertzuweisungen, a wird zu 12, b wird zu 44
    b = 1071;                                 // An der Speicherstelle a wird der Wert 12 gespeichert
    a_orig = a;
    b_orig = b;
    
    if (a == 0) {                           // Falls a den Wwert 0 hat, dann
       resultat = b;                        // ist ggT(a,b) = b
    } else {                                // sonst
       while (b != 0) {                     // repetiere solange b ungleich 0 ist folgendes
         if (a > b) {                       // falls a > b ist, dann
           a = a-b;                         // subtrahiere b von a
         } else {                           // sonst
           b = b-a;                         // subtrahiere a von b
         }
       }
       resultat = a;                        // Am Ende der Schleife steht in a der ggT(a,b)
    }
    System.out.println("Der ggT(" + a_orig + "," + b_orig + ") ist gleich " + resultat);
  }
}