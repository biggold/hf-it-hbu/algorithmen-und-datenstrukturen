// ArrayApp.java
class ArrayApp
   {
   public static void main(String[] args)
      {
      long[] arr;                  // Array-Referenz
      arr = new long[100];         // Erzeuge Array
      int nElems = 0;              // Anzahl Elemente
      int j;                       // Schleifenz�hler
      long searchKey;              // Suchschl�ssel
//--------------------------------------------------------------
      arr[0] = 77;                 // F�ge 10 Elemente ein
      arr[1] = 99;
      arr[2] = 44;
      arr[3] = 55;
      arr[4] = 22;
      arr[5] = 88;
      arr[6] = 11;
      arr[7] = 00;
      arr[8] = 66;
      arr[9] = 33;
      nElems = 10;                 // Nun sind 10 Elemente drin
//--------------------------------------------------------------
      for(j=0; j<nElems; j++)      // Zeige die Elemente
         System.out.print(arr[j] + " ");
      System.out.println("");
//--------------------------------------------------------------
      searchKey = 66;              // Finde Element mit Schl�ssel 66
      for(j=0; j<nElems; j++)          // F�r jedes Element,
         if(arr[j] == searchKey)       // gefunden?
            break;                     // Ja, aufh�ren
      if(j == nElems)                  // Am Ende?
         System.out.println("Finde nicht " + searchKey); // Ja
      else
         System.out.println("Gefunden " + searchKey);      // Nein
//--------------------------------------------------------------
      searchKey = 27;              // Finde Element mit Schl�ssel 27
      for(j=0; j<nElems; j++)          // F�r jedes Element,
         if(arr[j] == searchKey)       // gefunden?
            break;                     // Ja, aufh�ren
      if(j == nElems)                  // Am Ende?
         System.out.println("Finde nicht " + searchKey); // Ja
      else
         System.out.println("Gefunden " + searchKey);      // Nein
//--------------------------------------------------------------
      searchKey = 55;              // L�sche Element mit Schl�ssel 55
      System.out.println("L�sche nun " + searchKey);
      for(j=0; j<nElems; j++)           // Suche es
      if(arr[j] == searchKey)
         break;
      for(int k=j; k<nElems; k++)       // nachfolgende vorverschieben
         arr[k] = arr[k+1];
      nElems--;                         // Verringere die Anzahl Elemente
//--------------------------------------------------------------
      for(j=0; j<nElems; j++)      // Zeige die Elemente
         System.out.print( arr[j] + " ");
      System.out.println("");
      }  // end main()
   }  // end class ArrayApp
