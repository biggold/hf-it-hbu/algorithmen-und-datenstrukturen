// InfixApp.java
import java.io.*;            // for I/O
////////////////////////////////////////////////////////////////
class StackX
   {
   private int maxSize;
   private char[] stackArray;
   private int top;
//--------------------------------------------------------------
   public StackX(int s)       // Konstruktor
      {
      maxSize = s;
      stackArray = new char[maxSize];
      top = -1;
      }
//--------------------------------------------------------------
   public void push(char j)  // F�ge Element auf Stack ein
      { stackArray[++top] = j; }
//--------------------------------------------------------------
   public char pop()         // Entferne Element vom Stack
      { return stackArray[top--]; }
//--------------------------------------------------------------
   public char peek()        // Aktuelles Element im Stack
      { return stackArray[top]; }
//--------------------------------------------------------------
   public boolean isEmpty()  // True falls Stack leer
      { return (top == -1); }
//-------------------------------------------------------------
   public int size()         // Anzahl Elemente im Stack
      { return top+1; }
//--------------------------------------------------------------
   public char peekN(int n)  // Stack-Element an Stelle n
      { return stackArray[n]; }
//--------------------------------------------------------------
   public void displayStack(String s)
      {
      System.out.print(s);
      System.out.print("Stack (unten-->oben): ");
      for(int j=0; j<size(); j++)
         {
         System.out.print( peekN(j) );
         System.out.print(' ');
         }
      System.out.println("");
      }
//--------------------------------------------------------------
   }  // end class StackX
////////////////////////////////////////////////////////////////
class InToPost                  // Konversion von Infix nach Postfix
   {
   private StackX theStack;
   private String input;
   private String output = "";
//--------------------------------------------------------------
   public InToPost(String in)   // Konstruktor
      {
      input = in;
      int stackSize = input.length();
      theStack = new StackX(stackSize);
      }
//--------------------------------------------------------------
   public String doTrans()      // Umwandlung nach Postfix
      {
      for(int j=0; j<input.length(); j++)      // F�r jedes char
         {
         char ch = input.charAt(j);            // Hole es
         theStack.displayStack("For "+ch+" "); // *Diagnose*
         switch(ch)
            {
            case '+':               // Es ist + oder -
            case '-':
               gotOper(ch, 1);      // Pop Operator
               break;               //   (Rang 1)
            case '*':               // Es ist * or /
            case '/':
               gotOper(ch, 2);      // Pop Operator
               break;               //   (Rang 2)
            case '(':               // Es ist (
               theStack.push(ch);   // Push es
               break;
            case ')':               // Es ist )
               gotParen(ch);        // Pop operator
               break;
            default:                // Muss ein operand sein
               output = output + ch; // Schreibe es nach output
               break;
            }  // end switch
         }  // end for
      while( !theStack.isEmpty() )     // Pop restliche Operatoren
         {
         theStack.displayStack("While ");  // *Diagnose*
         output = output + theStack.pop(); // Schreibe nach output
         }
      theStack.displayStack("End   ");     // *Diagnose*
      return output;                   // In output ist Postfix-Ausdruck
      }  // end doTrans()
//--------------------------------------------------------------
   public  void gotOper(char opThis, int prec1)
      {                                // Erhielt Operator vom input
      while( !theStack.isEmpty() )
         {
         char opTop = theStack.pop();
         if( opTop == '(' )            // Falls es '(' ist
            {
            theStack.push(opTop);      // Restore '('
            break;
            }
         else                          // Es ist ein Operator
            {
            int prec2;                 // Rang der neuen Operation

            if(opTop=='+' || opTop=='-')  // Finde Rang der neuen Operation
               prec2 = 1;
            else
               prec2 = 2;
            if(prec2 < prec1)          // Falls rang neue Operation < alte Operation
               {                       //
               theStack.push(opTop);   // Rette die neue Operation
               break;
               }
            else                       // Rang der neuen Operation >= alte Operation
               output = output + opTop;  //
            }  // end else (it's an operator)
         }  // end while
      theStack.push(opThis);           // Push neuer Operator
      }  // end gotOp()
//--------------------------------------------------------------
   public  void gotParen(char ch)
      {                             // Erhielt ) von input
      while( !theStack.isEmpty() )
         {
         char chx = theStack.pop();
         if( chx == '(' )           // Falls '(' gepoppt
            break;                  // erledigt
         else                       // falls Operator gepoppt
            output = output + chx;  // output Operator
         }  // end while
      }  // end popOps()
//--------------------------------------------------------------
   }  // end class InToPost
////////////////////////////////////////////////////////////////
class InfixApp
   {
   public static void main(String[] args) throws IOException
      {
      String input, output;
      while(true)
         {
         System.out.print("Infix-Ausdruck eingeben: ");
         System.out.flush();
         input = getString();         // Lese String von der Tastatur
         if( input.equals("") )       // Ende falls [Enter]
            break;
                                      // Erzeuge ein translator-Objekt
         InToPost theTrans = new InToPost(input);
         output = theTrans.doTrans(); // �bersetze
         System.out.println("Postfix-Ausdruck ist " + output + '\n');
         }  // end while
      }  // end main()
//--------------------------------------------------------------
   public static String getString() throws IOException
      {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
      }
//--------------------------------------------------------------
   }  // end class InfixApp
////////////////////////////////////////////////////////////////
