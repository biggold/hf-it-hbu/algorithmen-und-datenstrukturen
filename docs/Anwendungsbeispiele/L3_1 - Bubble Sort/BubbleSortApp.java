// BubbleSortApp.java
////////////////////////////////////////////////////////////////
class ArrayBub
   {
   private long[] a;                 // Array-Referenz
   private int nElems;               // Anzahl Elemente
//--------------------------------------------------------------
   public ArrayBub(int max)          // Konstruktor
      {
      a = new long[max];                 // Erzeuge Array
      nElems = 0;                        // Noch keine Elemente
      }
//--------------------------------------------------------------
   public void insert(long value)    // Element in Array einf�gen
      {
      a[nElems] = value;             // Einf�gen
      nElems++;                      // Gr�sse Inkrementieren
      }
//--------------------------------------------------------------
   public void display()             // Zeige Array
      {
      for(int j=0; j<nElems; j++)       // F�r jedes Elemet
         System.out.print(a[j] + " ");  // Zeige es
      System.out.println("");
      }
//--------------------------------------------------------------
   public void BubbleSortApp()
      {
      int out, in;

      for(out=nElems-1; out>1; out--)   // �ussere Schleife (r�ckw�rts)
         for(in=0; in<out; in++)        // Innere Schleife(vorw�rts)
            if( a[in] > a[in+1] )       // Vergleich
               swap(in, in+1);          // Tausch
      }  // end BubbleSortApp()
//--------------------------------------------------------------
   private void swap(int one, int two)
      {
      long temp = a[one];
      a[one] = a[two];
      a[two] = temp;
      }
//--------------------------------------------------------------
   }  // end class ArrayBub
////////////////////////////////////////////////////////////////
class BubbleSortApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;            // Array-Gr�sse
      ArrayBub arr;                 // Array-Referenz
      arr = new ArrayBub(maxSize);  // Erzeuge Array

      arr.insert(77);               // F�ge 10 Elemente ein
      arr.insert(99);
      arr.insert(44);
      arr.insert(55);
      arr.insert(22);
      arr.insert(88);
      arr.insert(11);
      arr.insert(00);
      arr.insert(66);
      arr.insert(33);

      arr.display();                // Zeige Elemente

      arr.BubbleSortApp();             // Bubble Sort

      arr.display();                // Zeige Elemente
      }  // end main()
   }  // end class BubbleSortApp
////////////////////////////////////////////////////////////////
