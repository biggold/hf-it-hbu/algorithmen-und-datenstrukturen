// LowArray.java
////////////////////////////////////////////////////////////////
class LowArray
   {
   private long[] a;                 // Array-Referenz
//--------------------------------------------------------------
   public LowArray(int size)         // Konstruktor
      { a = new long[size]; }        // Erzeuge Array
//--------------------------------------------------------------
   public void setElem(int index, long value)  // Element setzen
      { a[index] = value; }
//--------------------------------------------------------------
   public long getElem(int index)              // Element holen
      { return a[index]; }
//--------------------------------------------------------------
   }  // end class LowArray
////////////////////////////////////////////////////////////////
class LowArrayApp
   {
   public static void main(String[] args)
      {
      LowArray arr;                 // Array-Referenz
      arr = new LowArray(100);      // Erzeuge LowArrayApp Objekt
      int nElems = 0;               // Anzahl Elemente im LowArrayApp
      int j;                        // Schleifenz�hler

      arr.setElem(0, 77);           // 10 Elemente einf�gen
      arr.setElem(1, 99);
      arr.setElem(2, 44);
      arr.setElem(3, 55);
      arr.setElem(4, 22);
      arr.setElem(5, 88);
      arr.setElem(6, 11);
      arr.setElem(7, 00);
      arr.setElem(8, 66);
      arr.setElem(9, 33);
      nElems = 10;                 // Nun sind 10 Elemente dirn

      for(j=0; j<nElems; j++)      // Zeige die Elemente
         System.out.print(arr.getElem(j) + " ");
      System.out.println("");

      int searchKey = 26;          // Suche das Element 26
      for(j=0; j<nElems; j++)            // F�r jedes Element
         if(arr.getElem(j) == searchKey) // Gefunden?
            break;
      if(j == nElems)                    // Nein
         System.out.println("Finde nicht " + searchKey);
      else                               // Ja
         System.out.println("Gefunden " + searchKey);

                                   // L�sche Element 55
      for(j=0; j<nElems; j++)            // Suche es
      if(arr.getElem(j) == 55)
         break;
      for(int k=j; k<nElems; k++)        // nachfolgende vorverschieben
         arr.setElem(k, arr.getElem(k+1) );
      nElems--;                          // verringere die Anzahl Elemente

      for(j=0; j<nElems; j++)      // Zeige die Elemente
         System.out.print( arr.getElem(j) + " ");
      System.out.println("");
      }  // end main()
   }  // end class LowArrayApp
////////////////////////////////////////////////////////////////
