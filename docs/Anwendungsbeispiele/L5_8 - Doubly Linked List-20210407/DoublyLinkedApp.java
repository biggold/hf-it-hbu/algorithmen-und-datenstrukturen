// DoublyLinkedApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public long dData;                 // Datensatz
   public Link next;                  // Referenz auf Nachfolger
   public Link previous;              // Referenz auf Vorg�nger
// -------------------------------------------------------------
   public Link(long d)                // Konstruktor
      { dData = d; }
// -------------------------------------------------------------
   public void displayLink()          // Zeige Inhalt des Link-Objekts
      { System.out.print(dData + " "); }
// -------------------------------------------------------------
   }  // end class Link
////////////////////////////////////////////////////////////////
class DoublyLinkedList
   {
   private Link first;               // Referenz zum ersten Link-Objekt
   private Link last;                // Referenz zum letzten Link-Objekt
// -------------------------------------------------------------
   public DoublyLinkedList()         // Konstruktor
      {
      first = null;                  // Noch keine Link-Objekte vorhanden
      last = null;
      }
// -------------------------------------------------------------
   public boolean isEmpty()          // True falls keine Link-Objekte vorhanden
      { return first==null; }
// -------------------------------------------------------------
   public void insertFirst(long dd)  // Einf�gen am Anfang
      {
      Link newLink = new Link(dd);   // Erzeuge neues Link-Objekt

      if( isEmpty() )                // Falls Liste leer,
         last = newLink;             // newLink <-- 'last'
      else
         first.previous = newLink;   // newLink <-- altes 'first'
      newLink.next = first;          // newLink --> altes 'first'
      first = newLink;               // 'first' --> newLink
      }
// -------------------------------------------------------------
   public void insertLast(long dd)   // Einf�gen am Ende
      {
      Link newLink = new Link(dd);   // Erzeuge neues Link-Objekt
      if( isEmpty() )                // Falls Liste leeer,
         first = newLink;            // 'first' --> newLink
      else
         {
         last.next = newLink;        // altes 'last' --> newLink
         newLink.previous = last;    // altes 'last' <-- newLink
         }
      last = newLink;                // newLink <-- 'last'
      }
// -------------------------------------------------------------
   public Link deleteFirst()         // L�schen am Anfang
      {                              // (Annahme: Liste nicht leer)
      Link temp = first;
      if(first.next == null)         // Falls nur ein Link-Objekt
         last = null;                // null <-- 'last'
      else
         first.next.previous = null; // null <-- altes 'next'
      first = first.next;            // 'first' --> altes 'next'
      return temp;
      }
// -------------------------------------------------------------
   public Link deleteLast()          // L�schen am Ende
      {                              // (Annahme: Liste nicht leer)
      Link temp = last;
      if(first.next == null)         // Falls nur ein Link-Objekt
         first = null;               // 'first' --> null
      else
         last.previous.next = null;  // altes 'previous' --> null
      last = last.previous;          // altes 'previous' <-- 'last'
      return temp;
      }
// -------------------------------------------------------------
                                     // Einf�gen von dd nach key
   public boolean insertAfter(long key, long dd)
      {                              // (Annahme: Liste nicht leer)
      Link current = first;          // Starte am Anfang
      while(current.dData != key)    // Bis �bereinstimmung gefunden,
         {
         current = current.next;     // Gehe zum n�chsten Link-Objekt
         if(current == null)
            return false;            // Konnte es nicht finden
         }
      Link newLink = new Link(dd);   // Mache neues Link-Objekt

      if(current==last)              // Falls letztes Link-Objekt,
         {
         newLink.next = null;        // newLink --> null
         last = newLink;             // newLink <-- 'last'
         }
      else                           // Nicht letztes Link-Objekt,
         {
         newLink.next = current.next; // newLink --> altes 'next'
                                      // newLink <-- altes 'next'
         current.next.previous = newLink;
         }
      newLink.previous = current;    // altes 'current' <-- newLink
      current.next = newLink;        // altes 'current' --> newLink
      return true;                   // Gefunden und eingef�gt
      }
// -------------------------------------------------------------
   public Link deleteKey(long key)   // L�sche Datensatz mit gegebenem key
      {                              // (Annahme: Liste nicht leer)
      Link current = first;          // Starte am Anfang
      while(current.dData != key)    // Bis �bereinstimmung gefunden,
         {
         current = current.next;     // Gehe zum n�chsten Link-Objekt
         if(current == null)
            return null;             // Konnte es nicht finden
         }
      if(current==first)             // Gefunden. Erstes Link-Objekt?
         first = current.next;       // 'first' --> altes 'next'
      else                           // Nicht erstes Link-Objekt
                                     // altes 'previous' --> altes 'next'
         current.previous.next = current.next;

      if(current==last)              // Letztes Link-Objekt?
         last = current.previous;    // altes 'previous' <-- 'last'
      else                           // Nicht letztes Link-Objekt
                                     // altes 'previous' <-- altes 'next'
         current.next.previous = current.previous;
      return current;                // Gebe Wert zur�ck
      }
// -------------------------------------------------------------
   public void displayForward()
      {
      System.out.print("Liste (Anfang-->Ende): ");
      Link current = first;          // Starte am Anfang
      while(current != null)         // Bis zum Listenende,
         {
         current.displayLink();      // Zeige Daten
         current = current.next;     // Gehe zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   public void displayBackward()
      {
      System.out.print("Liste (Ende-->Anfang): ");
      Link current = last;           // Starte am Ende
      while(current != null)         // Bis Listenanfang,
         {
         current.displayLink();      // Zeige Daten
         current = current.previous; // Gehe zum Vorg�nger
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   }  // end class DoublyLinkedList
////////////////////////////////////////////////////////////////
class DoublyLinkedApp
   {
   public static void main(String[] args)
      {                             // Mache eine neue Liste
      DoublyLinkedList theList = new DoublyLinkedList();

      theList.insertFirst(22);      // Einf�gen am Anfang
      theList.insertFirst(44);
      theList.insertFirst(66);

      theList.insertLast(11);       // Einf�gen am Ende
      theList.insertLast(33);
      theList.insertLast(55);

      theList.displayForward();     // Zeige vorw�rts
      theList.displayBackward();    // Zeige r�ckw�rts

      theList.deleteFirst();        // L�sche erstes Link-Objekt
      theList.deleteLast();         // L�sche letztes Link-Objekt
      theList.deleteKey(11);        // L�sche Link-Objekt mit key 11

      theList.displayForward();     // Zeige vorw�rts

      theList.insertAfter(22, 77);  // F�ge 77 nach 22 ein
      theList.insertAfter(33, 88);  // F�ge 88 nach 33

      theList.displayForward();     // Zeige vorw�rts
      }  // end main()
   }  // end class DoublyLinkedApp
////////////////////////////////////////////////////////////////
