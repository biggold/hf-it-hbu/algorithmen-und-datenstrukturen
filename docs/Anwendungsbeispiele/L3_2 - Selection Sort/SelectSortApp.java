// SelectSortApp.java
////////////////////////////////////////////////////////////////
class ArraySel
   {
   private long[] a;                 // Array-Referenz
   private int nElems;               // Anzahl Elemente
//--------------------------------------------------------------
   public ArraySel(int max)          // Konstruktor
      {
      a = new long[max];                 // Erzeuge Array
      nElems = 0;                        // Noch keine Elemente
      }
//--------------------------------------------------------------
   public void insert(long value)    // Element in Array einf�gen
      {
      a[nElems] = value;             // Einf�gen
      nElems++;                      // Gr�sse inkrementieren
      }
//--------------------------------------------------------------
   public void display()             // Zeige Array
      {
      for(int j=0; j<nElems; j++)       // F�r jedes Element
         System.out.print(a[j] + " ");  // zeige es
      System.out.println("");
      }
//--------------------------------------------------------------
   public void selectionSort()
      {
      int out, in, min;

      for(out=0; out<nElems-1; out++)   // �ussere Schleife
         {
         min = out;                     // Minimum
         for(in=out+1; in<nElems; in++) // Innere Schleife
            if(a[in] < a[min] )         // Falls min gr�sser,
                min = in;               // dann haben wir ein neues min
         swap(out, min);                // Tausche
         }  // end for(out)
      }  // end selectionSort()
//--------------------------------------------------------------
   private void swap(int one, int two)
      {
      long temp = a[one];
      a[one] = a[two];
      a[two] = temp;
      }
//--------------------------------------------------------------
   }  // end class ArraySel
////////////////////////////////////////////////////////////////
class SelectSortApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;            // Array-Gr�sse
      ArraySel arr;                 // Array-Referenz
      arr = new ArraySel(maxSize);  // Erzeuge Array

      arr.insert(77);               // F�ge 10 Elemente ein
      arr.insert(99);
      arr.insert(44);
      arr.insert(55);
      arr.insert(22);
      arr.insert(88);
      arr.insert(11);
      arr.insert(00);
      arr.insert(66);
      arr.insert(33);

      arr.display();                // Zeige Elemente

      arr.selectionSort();          // Selection Sort

      arr.display();                // Zeige Elemente
      }  // end main()
   }  // end class SelectSortApp
////////////////////////////////////////////////////////////////
