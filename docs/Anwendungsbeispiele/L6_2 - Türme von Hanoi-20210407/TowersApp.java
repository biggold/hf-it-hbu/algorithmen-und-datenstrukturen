// TowersApp.java
////////////////////////////////////////////////////////////////
class TowersApp
   {
   static int nDisks = 4;

   public static void main(String[] args)
      {
      doTowers(nDisks, 'A', 'B', 'C');
      }
   //-----------------------------------------------------------
   public static void doTowers(int topN,
                               char quelle, char temp, char ziel)
      {
      if(topN==1)
         System.out.println("Disk 1 von " + quelle + " nach "+ ziel);
      else
         {
         doTowers(topN-1, quelle, ziel, temp);   // Von quelle nach temp

         System.out.println("Disk " + topN +   // Bewege Boden
                            " von " + quelle + " nach "+ ziel);
         doTowers(topN-1, temp, quelle, ziel);   // Von temp nach ziel
         }
      }
//-------------------------------------------------------------
   }  // end class TowersApp
////////////////////////////////////////////////////////////////
