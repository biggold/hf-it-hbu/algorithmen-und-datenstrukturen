// TowersApp.java
////////////////////////////////////////////////////////////////
class TowersApp
   {
   static int nDisks = 3;

   public static void main(String[] args)
      {
      doTowers(nDisks, 'A', 'B', 'C');
      }
   //-----------------------------------------------------------
   public static void doTowers(int topN,
                               char quelle, char temp, char ziel)
      {
      if(topN==1)
         System.out.println("Disk 1 von " + quelle + " nach "+ ziel);
      else
         {
         // Von quelle nach temp

         System.out.println("Disk " + topN +   // Bewege Boden
                            " von " + quelle + " nach "+ ziel);
         // Von temp nach ziel
         }
      }
//-------------------------------------------------------------
   }  // end class TowersApp
////////////////////////////////////////////////////////////////
