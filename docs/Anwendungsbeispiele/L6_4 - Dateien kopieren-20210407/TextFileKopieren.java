// TextFileKopieren.java
import java.io.*;
////////////////////////////////////////////////////////////////
public class TextFileKopieren {

  public static void main(String[] args) throws IOException {

    String           inFileName = "C:\\Temp\\Test.txt";   // Festlegen Dateinamen Input-Datei, Annahme: Datei vorhanden
    File             inFile = new File(inFileName);       // 1. Datei-Objekt erzeugen (Input)
    BufferedReader   tfin = new BufferedReader(new FileReader(inFileName));   // Input-Datei lesend �ffnen
    
    String           outFileName = "C:\\Temp\\Test2.txt"; // Festlegen Dateinamen Output-Datei
    File             outFile = new File(outFileName);     // 2. Datei-Objekt erzeugen (Output)
    BufferedWriter   tfout = new BufferedWriter(new FileWriter(outFileName)); // Output-Datei schreibend �ffnen
    
    String           line;
    
    while ((line=tfin.readLine()) != null) {   // Solange das Dateiende der Input-Datei nicht erreicht ist,
                                               // lese Zeile aus Input-Datei
      System.out.println(line);                // gib zur Kontrolle Zeile auf Bildschirm aus
      tfout.write(line);                       // schreibe Zeile in Output-Datei
      tfout.newLine();                         // Zeilenumbruch in Output-Datai
    }
    tfin.close();                              // Schliesse Input-Datei
    tfout.close();                             // Schliesse Output-Datei
  }
  
}
////////////////////////////////////////////////////////////////