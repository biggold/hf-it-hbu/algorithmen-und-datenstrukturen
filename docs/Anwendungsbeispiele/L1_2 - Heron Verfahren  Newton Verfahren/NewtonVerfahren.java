import java.io.*;

public class NewtonVerfahren {

  static double wurzel(double a) {
    double xNeu = a;
    double xAlt = -1.0;
    
    while (Math.abs(xNeu-xAlt) > 1e-8) {
      xAlt = xNeu;
      xNeu = (xAlt + a/xAlt) / 2.0;
      System.out.println("----> " + xNeu);
    }
    
    return xNeu;
  }

  public static void main (String[] args) {
  
    System.out.println("Berechnung der Wurzel von 2");
    System.out.println();
    System.out.println("Resultat: " + wurzel(7.0));
    System.out.println();
    System.out.println("Ende des Programms");
  }
}
