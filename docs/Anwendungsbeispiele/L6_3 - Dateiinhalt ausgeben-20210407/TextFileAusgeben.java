// TextFileAusgabe.java
import java.io.*;
////////////////////////////////////////////////////////////////
public class TextFileAusgeben {

  public static void main(String[] args) throws IOException {

    String          fileName = "C:\\Temp\\Test.txt";    // Festlegen des Dateinamens, Annahme: Datei vorhanden
    File            inFile = new File(fileName);        // Datei-Objekt erzeugen
    BufferedReader  f = new BufferedReader(new FileReader(fileName)); // Datei lesend �ffnen und
    String          line;                                             // Referenz auf Datei setzen
    
    while ((line=f.readLine()) != null) {      // Solange das Dateiende nicht erreicht ist,
                                               // lese Zeile aus Datei
      System.out.println(line);                // und gib sie auf den Bildschirm aus
    }
    f.close();                                 // Datei schliessen
  }

}
////////////////////////////////////////////////////////////////