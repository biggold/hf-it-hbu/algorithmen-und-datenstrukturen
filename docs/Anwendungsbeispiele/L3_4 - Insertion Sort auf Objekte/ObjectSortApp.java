// ObjectSortApp.java
////////////////////////////////////////////////////////////////
class Person
   {
   private String lastName;
   private String firstName;
   private int age;
   //-----------------------------------------------------------
   public Person(String last, String first, int a)
      {                               // Konstruktor
      lastName = last;
      firstName = first;
      age = a;
      }
   //-----------------------------------------------------------
   public void displayPerson()
      {
      System.out.print(" Nachname: " + lastName);
      System.out.print(", Vorname:  " + firstName);
      System.out.println(", Alter: " + age);
      }
   //-----------------------------------------------------------
   public String getLast()           // Get Nachname
      { return lastName; }
   }  // end class Person
////////////////////////////////////////////////////////////////
class ArrayInOb
   {
   private Person[] a;               // Array-Referen
   private int nElems;               // Anzahl Datens�tze
//--------------------------------------------------------------
   public ArrayInOb(int max)         // Konstruktor
      {
      a = new Person[max];               // Erzeuge Array
      nElems = 0;                        // Noch keine Datens�tze
      }
//--------------------------------------------------------------
                                     // Person ins Array einf�gen
   public void insert(String last, String first, int age)
      {
      a[nElems] = new Person(last, first, age);
      nElems++;                          // Inkrementiere Array-Gr�sse
      }
//--------------------------------------------------------------
   public void display()             // Zeige Array-Inhalt
      {
      for(int j=0; j<nElems; j++)       // F�r jeden Datensatz
         a[j].displayPerson();          // Zeige es
      }
//--------------------------------------------------------------
   public void insertionSort()
      {
      int in, out;

      for(out=1; out<nElems; out++)
         {
         Person temp = a[out];       // out ist Trennlinie
         in = out;                   // Starte Versetzung bei out

         while(in>0 &&               // Bis ein kleinerer gefunden wird,
               a[in-1].getLast().compareTo(temp.getLast())>0)
            {
            a[in] = a[in-1];         // Versetze Datensatz nach rechts
            --in;                    // Eine Position nach links
            }
         a[in] = temp;               // F�ge markierten Datendatz ein
         }  // end for
      }  // end insertionSort()
//--------------------------------------------------------------
   }  // end class ArrayInOb
////////////////////////////////////////////////////////////////
class ObjectSortApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;             // Array-Gr�sse
      ArrayInOb arr;                 // Array-Referenz
      arr = new ArrayInOb(maxSize);  // Erzeuge Array

      arr.insert("Evans", "Patty", 24);
      arr.insert("Smith", "Doc", 59);
      arr.insert("Smith", "Lorraine", 37);
      arr.insert("Smith", "Paul", 37);
      arr.insert("Yee", "Tom", 43);
      arr.insert("Hashimoto", "Sato", 21);
      arr.insert("Stimson", "Henry", 29);
      arr.insert("Velasquez", "Jose", 72);
      arr.insert("Vang", "Minh", 22);
      arr.insert("Creswell", "Lucinda", 18);

      System.out.println("Before sorting:");
      arr.display();                 // Zeige Datens�tze

      arr.insertionSort();           // Insertion Sort

      System.out.println("After sorting:");
      arr.display();                 // Zeige Datens�tze erneut
      }  // end main()
   }  // end class ObjectSortApp
////////////////////////////////////////////////////////////////
