// SortedListApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public long dData;                  // Datensatz
   public Link next;                   // N�chstes Link-Objekt in der Liste
// -------------------------------------------------------------
   public Link(long dd)                // Konstruktor
      { dData = dd; }
// -------------------------------------------------------------
   public void displayLink()           // Zeige Link-Inhalt
      { System.out.print(dData + " "); }
   }  // end class Link
////////////////////////////////////////////////////////////////
class SortedList
   {
   private Link first;                 // Referenz auf erstes Link-Objekt
// -------------------------------------------------------------
   public SortedList()                 // Konstruktor
      { first = null; }
// -------------------------------------------------------------
   public boolean isEmpty()            // True falls Liste leer
      { return (first==null); }
// -------------------------------------------------------------
   public void insert(long key)        // Sortiert einf�gen
      {
      Link newLink = new Link(key);    // Mache neues Link-Objekt
      Link previous = null;            // Starte bei 'first'
      Link current = first;


      }  // end insert()
// -------------------------------------------------------------
   public Link remove()           // L�sche erstes Link-Objekt
      {                           // (Annahme: Liste nicht leer)
      Link temp = first;               // Rette 'first'
      first = first.next;              // L�sche 'first'
      return temp;                     // Gib Wert zur�ck
      }
// -------------------------------------------------------------
   public void displayList()
      {
      System.out.print("Liste (Anfang-->Ende): ");
      Link current = first;       // Starte am Anfang
      while(current != null)      // Bis das Listenende erreicht ist,
         {
         current.displayLink();   // Gig Daten aus
         current = current.next;  // Gehe zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
   }  // end class SortedList
////////////////////////////////////////////////////////////////
class SortedListApp
   {
   public static void main(String[] args)
      {                            // Erzeuge neue Liste
      SortedList theSortedList = new SortedList();
      theSortedList.insert(20);    // F�ge 2 Link-Objekte ein
      theSortedList.insert(40);

      theSortedList.displayList(); // Zeige Liste

      theSortedList.insert(10);    // F�ge 3 weitere Link-Objekte ein
      theSortedList.insert(30);
      theSortedList.insert(50);

      theSortedList.displayList(); // Zeige Liste

      theSortedList.remove();      // L�sche Link-Objekt

      theSortedList.displayList(); // Zeige Liste
      }  // end main()
   }  // end class SortedListApp
////////////////////////////////////////////////////////////////
