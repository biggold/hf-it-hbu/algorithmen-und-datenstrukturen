// PartitionApp.java
////////////////////////////////////////////////////////////////
class ArrayPar
   {
   private long[] theArray;          // Referenz auf Array
   private int nElems;               // Anzahl Datens�tze
   //--------------------------------------------------------------
   public ArrayPar(int max)          // Konstruktor
      {
      theArray = new long[max];      // Erzeuge Array
      nElems = 0;                    // Noch keine Datens�tze
      }
//--------------------------------------------------------------
   public void insert(long value)    // Datensatz einf�gen
      {
      theArray[nElems] = value;      // F�ge es ein
      nElems++;                      // Inkrementiere Array-Gr�sse
      }
//--------------------------------------------------------------
   public int size()                 // Gib Anzahl Datens�tze zur�ck
      { return nElems; }
//--------------------------------------------------------------
   public void display()             // Zeige Array-Inhalt
      {
      System.out.print("A=");
      for(int j=0; j<nElems; j++)    // F�r jeden Datensatz,
         System.out.print(theArray[j] + " ");  // Zeige es
      System.out.println("");
      }
//--------------------------------------------------------------
    public int partitionIt(int left, int right, long pivot)
       {
       int leftPtr = left - 1;           // Links vom ersten Element
       int rightPtr = right + 1;         // Rechts vom letzten Element

       return leftPtr;                   // Gib Position mit Wert>=Pivot zur�ck
       }  // end partitionIt()
//--------------------------------------------------------------
   public void swap(int dex1, int dex2)  // Swap zwei Elemente
      {
      long temp;
      temp = theArray[dex1];             // A nach temp
      theArray[dex1] = theArray[dex2];   // B nach A
      theArray[dex2] = temp;             // temp nach B
      }  // end swap()
//--------------------------------------------------------------
   }  // end class ArrayPar
////////////////////////////////////////////////////////////////
class PartitionApp
   {
   public static void main(String[] args)
      {
      int maxSize = 16;             // Array-Gr�sse
      ArrayPar arr;                 // Array-Referenz
      arr = new ArrayPar(maxSize);  // Erzeuge Array

      for(int j=0; j<maxSize; j++)  // F�lle Array mit
         {                          // Zufallszahlen
         long n = (int)(java.lang.Math.random()*199);
         arr.insert(n);
         }
      arr.display();                // Zeige unsortiertes Array

      long pivot = 99;              // Setze Pivot
      System.out.print("Pivot ist " + pivot);
      int size = arr.size();
                                    // partitionIt Array
      int partDex = arr.partitionIt(0, size-1, pivot);

      System.out.println(", Partition ist an Indexposition " + partDex);
      arr.display();                // Zeige partitioniertes Array
      }  // end main()
   }  // end class PartitionApp
////////////////////////////////////////////////////////////////
