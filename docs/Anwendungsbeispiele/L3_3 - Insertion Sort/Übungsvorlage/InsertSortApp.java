// InsertSortApp.java
//--------------------------------------------------------------
class ArrayIns
   {
   private long[] a;                 // Array-Referenz
   private int nElems;               // Anzahl Datens�tze
//--------------------------------------------------------------
   public ArrayIns(int max)          // Konstruktor
      {
      a = new long[max];                 // Erzeuge Array
      nElems = 0;                        // Noch keine Datens�tze
      }
//--------------------------------------------------------------
   public void insert(long value)    // Datensatz einf�gen
      {
      a[nElems] = value;             // F�ge es ein
      nElems++;                      // Erh�he die Anzahl Datens�tze
      }
//--------------------------------------------------------------
   public void display()             // Zeige Array-Inhalt
      {
      for(int j=0; j<nElems; j++)       // F�r jeden Datensatz
         System.out.print(a[j] + " ");  // Zeige es
      System.out.println("");
      }
//--------------------------------------------------------------
   public void insertionSort()
      {

      }  // end insertionSort()
//--------------------------------------------------------------
   }  // end class ArrayIns
////////////////////////////////////////////////////////////////
class InsertSortApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;            // Array-Gr�sse
      ArrayIns arr;                 // Array-Referenz
      arr = new ArrayIns(maxSize);  // Erzeuge Array

      arr.insert(77);               // F�ge 10 Datens�tze ein
      arr.insert(99);
      arr.insert(44);
      arr.insert(55);
      arr.insert(22);
      arr.insert(88);
      arr.insert(11);
      arr.insert(00);
      arr.insert(66);
      arr.insert(33);

      arr.display();                // Zeige Datens�tze

      arr.insertionSort();          // Insertion Sort

      arr.display();                // Zeige Datens�tze erneut
      }  // end main()
   }  // end class InsertSortApp
