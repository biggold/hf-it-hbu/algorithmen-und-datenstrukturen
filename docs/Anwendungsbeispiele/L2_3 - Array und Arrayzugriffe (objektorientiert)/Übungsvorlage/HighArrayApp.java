// HighArray.java
////////////////////////////////////////////////////////////////
class HighArray
   {
   private long[] a;                 // Array-Referenz
   private int nElems;               // Anzahl Elemente
   //-----------------------------------------------------------
   public HighArray(int max)         // Konstruktor
      {
      a = new long[max];                 // Erzeuge Array
      nElems = 0;                        // Anzahl Elemente = 0
      }
   //-----------------------------------------------------------
   public boolean find(long searchKey)
      {                              // Finde Element
      int j;
      for(j=0; j<nElems; j++)            // F�r jedes Element,
         if(a[j] == searchKey)           // Gefunden?
            break;                       // Abbrechen
      if(j == nElems)                    // Ende erreicht?
         return false;                   // Ja, kann es nicht finden
      else
         return true;                    // Nein, gefunden
      }  // end find()
   //-----------------------------------------------------------
   public void insert(long value)    // Element in Array einf�gen
      {
      a[nElems] = value;             // Einf�gen
      nElems++;                      // Anzahl inkrementieren
      }
   //-----------------------------------------------------------
   public boolean delete(long value)
      {
      int j;
      for(j=0; j<nElems; j++)        // Suche es
         if( value == a[j] )
            break;
      if(j==nElems)                  // Konnte es nicht finden
         return false;
      else                           // Gefunden
         {
         for(int k=j; k<nElems; k++) // Nachfolgende Elemente vorverschieben
            a[k] = a[k+1];
         nElems--;                   // Anzahl dekrementieren
         return true;
         }
      }  // end delete()
   //-----------------------------------------------------------
   public void display()             // Array-Inhalt anzeigen
      {
      for(int j=0; j<nElems; j++)       // F�r jedes Element,
         System.out.print(a[j] + " ");  // Zeige es
      System.out.println("");
      }
   //-----------------------------------------------------------
   }  // end class HighArray
////////////////////////////////////////////////////////////////
class HighArrayApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;            // Array-Gr�sse
      HighArray arr;                // Array-Referenz
      arr = new HighArray(maxSize); // Erzeuge Array mit gew�nschter Gr�sse

      arr.insert(77);               // F�ge 10 Elemente ein
      arr.insert(99);
      arr.insert(44);
      arr.insert(55);
      arr.insert(22);
      arr.insert(88);
      arr.insert(11);
      arr.insert(00);
      arr.insert(66);
      arr.insert(33);

      arr.display();                // Zeige den Array-Inhalt

      int searchKey = 35;           // Suche Element 35
      if( arr.find(searchKey) )
         System.out.println("Gefunden " + searchKey);
      else
         System.out.println("Finde nicht " + searchKey);

      arr.delete(00);               // L�sche drei Elemente (00, 55, 99)
      arr.delete(55);
      arr.delete(99);

      arr.display();                // Zeige den Array-Inhalt erneut
      }  // end main()
   }  // end class HighArrayApp
