// MergeSortApp.java
////////////////////////////////////////////////////////////////
class DArray
   {
   private long[] theArray;          // Referenz auf theArray
   private int nElems;               // Anzahl Datens�tze
   //-----------------------------------------------------------
   public DArray(int max)            // Konstruktor
      {
      theArray = new long[max];      // Erzeuge Array
      nElems = 0;
      }
   //-----------------------------------------------------------
   public void insert(long value)    // F�ge Element in Array ein
      {
      theArray[nElems] = value;      // Einf�gen
      nElems++;                      // Inkrementiere Array-Gr�sse
      }
   //-----------------------------------------------------------
   public void display()             // Zeige Array
      {
      for(int j=0; j<nElems; j++)    // F�r jeden Datensatz,
         System.out.print(theArray[j] + " ");  // Zeige es
      System.out.println("");
      }
   //-----------------------------------------------------------
   public void mergeSort()           // Von main() aufgerufen
      {                              // Erzeugt workspace-Array
      long[] workSpace = new long[nElems];
      recMergeSort(workSpace, 0, nElems-1);
      }
   //-----------------------------------------------------------
   private void recMergeSort(long[] workSpace, int lowerBound,
                                               int upperBound)
      {
      if(lowerBound == upperBound)            // Falls Bereich 1 Element,
         return;                              // Keine Sortierung n�tig
      else
         {                                    // Finde Bereichsmitte
         int mid = (lowerBound+upperBound) / 2;
                                              // Sortiere untere H�lfte
         recMergeSort(workSpace, lowerBound, mid);
                                              // Sortiere obere H�lfte
         recMergeSort(workSpace, mid+1, upperBound);
                                              // Mische Arrays
         merge(workSpace, lowerBound, mid+1, upperBound);
         }  // end else
      }  // end recMergeSort()
   //-----------------------------------------------------------
   private void merge(long[] workSpace, int lowPtr,
                           int highPtr, int upperBound)
      {
      int j = 0;                             // workspace-Array Index
      int lowerBound = lowPtr;
      int mid = highPtr-1;
      int n = upperBound-lowerBound+1;       // Anzahl Datens�tze

      while(lowPtr <= mid && highPtr <= upperBound)
         if( theArray[lowPtr] < theArray[highPtr] )
            workSpace[j++] = theArray[lowPtr++];
         else
            workSpace[j++] = theArray[highPtr++];

      while(lowPtr <= mid)
         workSpace[j++] = theArray[lowPtr++];

      while(highPtr <= upperBound)
         workSpace[j++] = theArray[highPtr++];

      for(j=0; j<n; j++)
         theArray[lowerBound+j] = workSpace[j];
      }  // end merge()
   //-----------------------------------------------------------
   }  // end class DArray
////////////////////////////////////////////////////////////////
class MergeSortApp
   {
   public static void main(String[] args)
      {
      int maxSize = 100;             // Array-Gr�sse
      DArray arr;                    // Referenz auf Array
      arr = new DArray(maxSize);     // Erzeuge Array

      arr.insert(64);                // F�ge Datens�tze ein
      arr.insert(21);
      arr.insert(33);
      arr.insert(70);
      arr.insert(12);
      arr.insert(85);
      arr.insert(44);
      arr.insert(3);
      arr.insert(99);
      arr.insert(0);
      arr.insert(108);
      arr.insert(36);

      arr.display();                 // Zeige Datens�tze

      arr.mergeSort();               // Merge Sort

      arr.display();                 // Ziege Datens�tze erneut
      }  // end main()
   }  // end class MergeSortApp
////////////////////////////////////////////////////////////////
