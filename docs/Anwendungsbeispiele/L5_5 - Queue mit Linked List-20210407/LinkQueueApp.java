// LinkQueueApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public long dData;                // Datensatz
   public Link next;                 // Referenz auf n�chstes Link-Objekt
// -------------------------------------------------------------
   public Link(long d)               // Konstruktor
      { dData = d; }
// -------------------------------------------------------------
   public void displayLink()         // Zeige dich
      { System.out.print(dData + " "); }
// -------------------------------------------------------------
   }  // end class Link
////////////////////////////////////////////////////////////////
class FirstLastList
   {
   private Link first;               // Referenz auf 'first' Link-Objekt
   private Link last;                // Referenz auf 'last' Link-Objekt
// -------------------------------------------------------------
   public FirstLastList()            // Konstruktor
      {
      first = null;                  // Noch keine Link-Objekte vorhanden
      last = null;
      }
// -------------------------------------------------------------
   public boolean isEmpty()          // True falls keine Link-Objekte vorhanden
      { return first==null; }
// -------------------------------------------------------------
   public void insertLast(long dd) // Einf�gen am Ende der Liste
      {
      Link newLink = new Link(dd);   // Erzeuge neues Link-Objekt
      if( isEmpty() )                // Falls Liste leer,
         first = newLink;            // 'first' --> newLink
      else
         last.next = newLink;        // altes 'last' --> newLink
      last = newLink;                // newLink <-- 'last'
      }
// -------------------------------------------------------------
   public long deleteFirst()         // L�schen des ersten Link-Objekts
      {                              // (Annahme: Liste nicht leer)
      long temp = first.dData;
      if(first.next == null)         // Falls nur ein Link-Objekt
         last = null;                // null <-- 'last'
      first = first.next;            // 'first' --> altes next
      return temp;
      }
// -------------------------------------------------------------
   public void displayList()
      {
      Link current = first;          // Starte am Anfang
      while(current != null)         // So lange das Ende nicht erreicht ist,
         {
         current.displayLink();      // Zeige Datensatz
         current = current.next;     // Bewege zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   }  // end class FirstLastList
////////////////////////////////////////////////////////////////
class LinkQueue
   {
   private FirstLastList theList;
//--------------------------------------------------------------
   public LinkQueue()                // Konstruktor
      { theList = new FirstLastList(); }  // Erzeuge Liste
//--------------------------------------------------------------
   public boolean isEmpty()          // True falls Liste leer
      { return theList.isEmpty(); }
//--------------------------------------------------------------
   public void insert(long j)        // F�ge am Ende ein
      { theList.insertLast(j); }
//--------------------------------------------------------------
   public long remove()              // Entferne am Anfang
      {  return theList.deleteFirst();  }
//--------------------------------------------------------------
   public void displayQueue()
      {
      System.out.print("Queue (Anfang-->Ende): ");
      theList.displayList();
      }
//--------------------------------------------------------------
   }  // end class LinkQueue
////////////////////////////////////////////////////////////////
class LinkQueueApp
   {
   public static void main(String[] args)
      {
      LinkQueue theQueue = new LinkQueue();
      theQueue.insert(20);                 // F�ge Datens�tze ein
      theQueue.insert(40);

      theQueue.displayQueue();             // Zeige Queue

      theQueue.insert(60);                 // F�ge Datens�tze ein
      theQueue.insert(80);

      theQueue.displayQueue();             // Zeige Queue

      theQueue.remove();                   // Entferne Datens�tze
      theQueue.remove();

      theQueue.displayQueue();             // Zeige Queue
      }  // end main()
   }  // end class LinkQueueApp
////////////////////////////////////////////////////////////////
