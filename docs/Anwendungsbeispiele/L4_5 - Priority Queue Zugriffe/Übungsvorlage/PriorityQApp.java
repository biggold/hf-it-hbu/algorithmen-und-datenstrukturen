// PriorityQ.java
////////////////////////////////////////////////////////////////
class PriorityQ
   {
   // Sortiertes Array, max bei 0 und min bei size-1
   private int maxSize;
   private long[] queArray;
   private int nItems;
//-------------------------------------------------------------
   public PriorityQ(int s)          // Konstruktor
      {
      maxSize = s;
      queArray = new long[maxSize];
      nItems = 0;
      }
//-------------------------------------------------------------
   public void insert(long item)    // Element einf�gen
      {

      }  // end insert()
//-------------------------------------------------------------
   public long remove()             // Kleinstes Element entfernen
      { return queArray[--nItems]; }
//-------------------------------------------------------------
   public long peekMin()            // Kleinstes Element lesen
      { return queArray[nItems-1]; }
//-------------------------------------------------------------
   public boolean isEmpty()         // True falls leer
      { return (nItems==0); }
//-------------------------------------------------------------
   public boolean isFull()          // True falls voll
      { return (nItems == maxSize); }
//-------------------------------------------------------------
   }  // end class PriorityQ
////////////////////////////////////////////////////////////////
class PriorityQApp
   {
   public static void main(String[] args)
      {
      PriorityQ thePQ = new PriorityQ(5);
      thePQ.insert(30);
      thePQ.insert(50);
      thePQ.insert(10);
      thePQ.insert(40);
      thePQ.insert(20);

      while( !thePQ.isEmpty() )
         {
         long item = thePQ.remove();
         System.out.print(item + " ");  // 10, 20, 30, 40, 50
         }  // end while
      System.out.println("");
      }  // end main()
//-------------------------------------------------------------
   }  // end class PriorityQApp
////////////////////////////////////////////////////////////////
