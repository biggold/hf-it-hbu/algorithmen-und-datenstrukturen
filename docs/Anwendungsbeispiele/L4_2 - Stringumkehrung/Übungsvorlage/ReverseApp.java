// ReverseApp.java
import java.io.*;                 // for I/O
////////////////////////////////////////////////////////////////
class StackX
   {
   private int maxSize;
   private char[] stackArray;
   private int top;
//--------------------------------------------------------------
   public StackX(int max)    // Konstruktor
      {
      maxSize = max;
      stackArray = new char[maxSize];
      top = -1;
      }
//--------------------------------------------------------------
   public void push(char j)  // Datensatz in Stack einf�gen
      {
      stackArray[++top] = j;
      }
//--------------------------------------------------------------
   public char pop()         // Datensatz von Stack entfernen
      {
      return stackArray[top--];
      }
//--------------------------------------------------------------
   public char peek()        // Aktueller Datensatz im Stack
      {
      return stackArray[top];
      }
//--------------------------------------------------------------
   public boolean isEmpty()  // True falls Stack leer
      {
      return (top == -1);
      }
//--------------------------------------------------------------
   }  // end class StackX
////////////////////////////////////////////////////////////////
class Reverser
   {
   private String input;                // Input String
   private String output;               // Output String
//--------------------------------------------------------------
   public Reverser(String in)           // Konstruktor
      { input = in; }
//--------------------------------------------------------------
   public String doRev()                // Kehre String um
      {
      int stackSize = input.length();   // Stack-Gr�sse festlegen
      StackX theStack = new StackX(stackSize);  // Stack erzeugen

      output = "";

      return output;
      }  // end doRev()
//--------------------------------------------------------------
   }  // end class Reverser
////////////////////////////////////////////////////////////////
class ReverseApp
   {
   public static void main(String[] args) throws IOException
      {
      String input, output;
      while(true)
         {
         System.out.print("String eingeben: ");
         System.out.flush();
         input = getString();          // Lese String von Tastatur
         if( input.equals("") )        // Ende falls [Enter]
            break;
                                       // Erzeuge ein Reverser-Objekt
         Reverser theReverser = new Reverser(input);
         output = theReverser.doRev(); // Verwende es
         System.out.println("Umkehrung: " + output);
         }  // end while
      }  // end main()
//--------------------------------------------------------------
   public static String getString() throws IOException
      {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String s = br.readLine();
      return s;
      }
//--------------------------------------------------------------
   }  // end class ReverseApp
////////////////////////////////////////////////////////////////
