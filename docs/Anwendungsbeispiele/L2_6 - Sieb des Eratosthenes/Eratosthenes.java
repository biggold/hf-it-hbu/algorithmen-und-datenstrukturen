// Eratosthenes.java
////////////////////////////////////////////////////////////////
class Eratosthenes {

public static void runEratosthenesSieb(int upperBound) {

      boolean[] isPrime = new boolean[upperBound + 1];
      int       k, m;
      
      for (m = 0; m <= upperBound; m++) {
        isPrime[m] = true;
      }
      m=2;
      while (m <= (upperBound-m)) {
            for (k = m+m; k <= upperBound; k += m) {
                  isPrime[k] = false;
            }
            do {
                  m++;
            } while (!isPrime[m] && (m < upperBound));
      }
      for (m = 2; m <= upperBound-1; m++) {
            if (isPrime[m]) {
                  System.out.print(m + " ");
            }
      }
}

public static void main(String[] args) {
       runEratosthenesSieb(200);
}
      
} // end class Eratosthenes
////////////////////////////////////////////////////////////////
