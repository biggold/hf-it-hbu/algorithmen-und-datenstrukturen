// LinkListApp2.java
////////////////////////////////////////////////////////////////
class Link
   {
   public int iData;              // Datenelement (Schl�ssel)
   public double dData;           // Datenelement
   public Link next;              // Link
// -------------------------------------------------------------
   public Link(int id, double dd) // Konstruktor
      {
      iData = id;                 // Initialisiere die Datenelemente
      dData = dd;                 // ('next' wird automatisch auf 'null'
      }                           //  gesetzt)
// -------------------------------------------------------------
   public void displayLink()      // Zeige dich
      {
      System.out.print("{" + iData + ", " + dData + "} ");
      }
   }  // end class Link
////////////////////////////////////////////////////////////////
class LinkList
   {
   private Link first;            // Referenz auf den ersten Link

// -------------------------------------------------------------
   public LinkList()              // Konstruktor
      {
      first = null;               // Noch keine Links vorhanden
      }
// -------------------------------------------------------------
                                  // Einf�gen am Anfang
   public void insertFirst(int id, double dd)
      {                           // Erzeuge neues Link-Objekt
      Link newLink = new Link(id, dd);
      newLink.next = first;       // newLink --> altes first
      first = newLink;            // first --> newLink
      }
// -------------------------------------------------------------
   public Link find(int key)      // Finde Datensatz mit Schl�ssel
      {                           // (Annahme: Liste nicht leer)
      return null;
      }
// -------------------------------------------------------------
   public Link delete(int key)    // L�sche Datensatz mit Schl�ssel
      {                           // (Annahme: Liste nicht leer)
      return null;
      }
// -------------------------------------------------------------
   public void displayList()
      {
      System.out.print("Liste (Anfang-->Ende): ");
      Link current = first;       // Starte am Anfang
      while(current != null)      // Solange bis das Ende erreicht wird,
         {
         current.displayLink();   // Zeige Datenelemente
         current = current.next;  // Bewege zum n�chsten Link-Objekt
         }
      System.out.println("");
      }
// -------------------------------------------------------------
   }  // end class LinkList
////////////////////////////////////////////////////////////////
class LinkListApp2
   {
   public static void main(String[] args)
      {
      LinkList theList = new LinkList();  // Erzeuge neue Liste

      theList.insertFirst(22, 2.99);      // F�ge 4 Datens�tze ein
      theList.insertFirst(44, 4.99);
      theList.insertFirst(66, 6.99);
      theList.insertFirst(88, 8.99);

      theList.displayList();              // Zeige die Liste

      Link f = theList.find(44);          // Suche Datensatz
      if( f != null)
         System.out.println("Gefunden Datensatz mit Schl�ssel " + f.iData);
      else
         System.out.println("Finde es nicht");

      Link d = theList.delete(66);        // L�sche Datensatz
      if( d != null )
         System.out.println("Gel�scht Datensatz mit Schl�ssel " + d.iData);
      else
         System.out.println("Kann es nicht l�schen");

      theList.displayList();              // Zeige Liste
      }  // end main()
   }  // end class LinkListApp2
////////////////////////////////////////////////////////////////
