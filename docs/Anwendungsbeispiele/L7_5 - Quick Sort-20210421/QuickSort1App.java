// QuickSort1App.java
////////////////////////////////////////////////////////////////
class ArrayIns
   {
   private long[] theArray;          // Array-Referenz
   private int nElems;               // Anzahl Datens�tze
//--------------------------------------------------------------
   public ArrayIns(int max)          // Konstruktor
      {
      theArray = new long[max];      // Erzeuge Array
      nElems = 0;                    // Noch keine Datens�tze
      }
//--------------------------------------------------------------
   public void insert(long value)    // Datensatz einf�gen
      {
      theArray[nElems] = value;      // F�ge es ein
      nElems++;                      // Inkrementiere Array-Gr�sse
      }
//--------------------------------------------------------------
   public void display()             // Zeige Array-Inhalt
      {
      System.out.print("A=");
      for(int j=0; j<nElems; j++)    // F�r jeden Datensatz,
         System.out.print(theArray[j] + " ");  // Zeige es
      System.out.println("");
      }
//--------------------------------------------------------------
   public void quickSort()
      {
      recQuickSort(0, nElems-1);
      }
//--------------------------------------------------------------
   public void recQuickSort(int left, int right)
      {
      if(right-left <= 0)              // Falls Gr�sse <= 1,
          return;                      //    bereits sortiert
      else                             // Gr�sse 2 oder mehr
         {
         long pivot = theArray[right];      // Datensatz rechts wird Pivot

         int partition = partitionIt(left, right, pivot);
         recQuickSort(left, partition-1);   // Sortiere linkes Teil-Array
         recQuickSort(partition+1, right);  // Sortiere rechtes Teil-Array
         }
      }  // end recQuickSort()
//--------------------------------------------------------------
    public int partitionIt(int left, int right, long pivot)
       {
       int leftPtr = left-1;           // left    (nach ++)
       int rightPtr = right;           // right-1 (nach --)
       while(true)
          {                            // Finde gr�sseren Datensatz
          while( theArray[++leftPtr] < pivot )
             ;  // (nop)
                                       // Finde kleineren Datensatz
          while(rightPtr > 0 && theArray[--rightPtr] > pivot)
             ;  // (nop)

          if(leftPtr >= rightPtr)      // Falls sich die Zeiger kreuzen,
             break;                    //    Partitionierung fertig
          else                         // Falls sie sich nicht kreuzen,
             swap(leftPtr, rightPtr);  //    Swap Datens�tze
          }  // end while(true)
       swap(leftPtr, right);           // Swap Pivot an die richtige Position
       return leftPtr;                 // Gib Position des Pivot zur�ck
       }  // end partitionIt()
//--------------------------------------------------------------
   public void swap(int dex1, int dex2)  // Swap zweier Datens�tze
      {
      long temp = theArray[dex1];        // A nach temp
      theArray[dex1] = theArray[dex2];   // B nach A
      theArray[dex2] = temp;             // temp nach B
      }  // end swap(
//--------------------------------------------------------------
   }  // end class ArrayIns
////////////////////////////////////////////////////////////////
class QuickSort1App
   {
   public static void main(String[] args)
      {
      int maxSize = 16;             // Array-Gr�sse
      ArrayIns arr;
      arr = new ArrayIns(maxSize);  // Erzeuge Array

      for(int j=0; j<maxSize; j++)  // F�lle Array mit
         {                          // Zufallszahlen
         long n = (int)(java.lang.Math.random()*99);
         arr.insert(n);
         }
      arr.display();                // Zeige Datens�tze
      arr.quickSort();              // Quick Sort
      arr.display();                // Zeige Datens�tze erneut
      }  // end main()
   }  // end class QuickSort1App
////////////////////////////////////////////////////////////////
