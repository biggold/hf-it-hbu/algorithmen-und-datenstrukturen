// ListInsertionSortApp.java
////////////////////////////////////////////////////////////////
class Link
   {
   public long dData;                  // Datensatz
   public Link next;                   // Referenz auf n�chstes Link-Objekt
// -------------------------------------------------------------
   public Link(long dd)                // Konstruktor
      { dData = dd; }
// -------------------------------------------------------------
   }  // end class Link
////////////////////////////////////////////////////////////////
class SortedList
   {
   private Link first;            // Referenz zum ersten Link-Objekt
// -------------------------------------------------------------
   public SortedList()            // Konstruktor (ohne Argument)
      { first = null; }                    // Initialisiere Liste
// -------------------------------------------------------------
   public SortedList(Link[] linkArr)  // Konstruktor (Array
      {                               // als Argument)
      first = null;                        // Initialisiere Liste
      for(int j=0; j<linkArr.length; j++)  // Kopiere Array
         insert( linkArr[j] );             // in Liste
      }
// -------------------------------------------------------------
   public void insert(Link k)     // Sortiertes Einf�gen
      {
      Link previous = null;            // Starte bei 'first'
      Link current = first;
                                       // Bis zum Listenende,
      while(current != null && k.dData > current.dData)
         {                             // Oder key > current,
         previous = current;
         current = current.next;       // Gehe zum n�chsten Link-Objekt
         }
      if(previous==null)               // Am Anfang der Liste
         first = k;                    // 'first' --> k
      else                             // Nicht am Anfang
         previous.next = k;            // Altes prev --> k
      k.next = current;                // k --> Altes current
      }  // end insert()
// -------------------------------------------------------------
   public Link remove()           // L�sche 'first' und gib es zur�ck
      {                           // (Annahme: Liste nicht leer)
      Link temp = first;               // Rette 'first'
      first = first.next;              // L�sche 'first'
      return temp;                     // Gib Wert zur�ck
      }
// -------------------------------------------------------------
   }  // end class SortedList
////////////////////////////////////////////////////////////////
class ListInsertionSortApp
   {
   public static void main(String[] args)
      {
      int size = 10;
                                 // Erzeugt Array of Links
      Link[] linkArray = new Link[size];

      for(int j=0; j<size; j++)  // F�lle Array mit Link-Objekten
         {                            // Zufallszahl
         int n = (int)(java.lang.Math.random()*99);
         Link newLink = new Link(n);  // Erzeuge Link-Objekt
         linkArray[j] = newLink;      // Speichere es im Array
         }
                                 // Zeige Array-Inhalt
      System.out.print("Unsortiertes Array: ");
      for(int j=0; j<size; j++)
         System.out.print( linkArray[j].dData + " " );
      System.out.println("");

                                 // Erzeuge neue Liste
      SortedList theSortedList = new SortedList(linkArray);

      for(int j=0; j<size; j++)  // Link-Objekte von Liste ins Array kopieren
         linkArray[j] = theSortedList.remove();

                                 // Zeige Array-Inhalt
      System.out.print("Sortiertes Array:   ");
      for(int j=0; j<size; j++)
         System.out.print(linkArray[j].dData + " ");
      System.out.println("");
      }  // end main()
   }  // end class ListInsertionSortApp
////////////////////////////////////////////////////////////////
