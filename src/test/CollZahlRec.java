package test;

import java.io.IOException;
import java.util.Stack;

public class CollZahlRec {
    //--------------------------------------------------------------
    public static int f(int n) {
        if ((n % 2) == 0) {
            return (n / 2);
        } else {
            return (3*n + 1);
        }
    }
    //--------------------------------------------------------------
    public static void g(int n,  Stack<Integer> theStack) {
        if (n == 1) {
            theStack.push(1);
        } else {
            theStack.push(n);
            g(f(n), theStack);
        }
    }
    //--------------------------------------------------------------
    public static void main(String[] args) throws IOException {
        Stack<Integer> theStack = new Stack<>();

        g(5, theStack);
        for(int elm : theStack) {
            System.out.println(elm);
        }
        System.out.println(theStack.toString());

    }
}
