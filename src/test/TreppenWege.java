package test;

public class TreppenWege {
    public static void main(String args[]) {
        System.out.println(wege(10));
    }

    public static int wege(int stufen) {
        if (stufen == 1) {
            return 1;
        } else if (stufen == 2) {
            return 2;
        } else {
            return wege(stufen - 1) + wege(stufen - 2);
        }
    }
}
