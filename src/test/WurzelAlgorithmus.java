package test;

/*
    NewtonVerfahren
 */

public class WurzelAlgorithmus {

    public static void main(String args[]) {
        long startTime = System.currentTimeMillis();
        wurzel((double) 148431148431684841384.0);
        long endTime = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime-startTime) + "ms");

        long startTime2 = System.currentTimeMillis();
        wurzelSolution((double) 148431148431684841384.0);
        long endTime2 = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime2-startTime2) + "ms");
    }
    /*
        My Solution
     */
    public static double wurzel(double A) {
        double y, yneu;
        y = 1;
        while (y * y != A) {
            System.out.println("Y: " + y + ", Fläche: " + A);
            yneu = (y + A / y) / 2;
            if (yneu == y) {
                // We have a problem here, we can't get more precise so this should be the result
                break;
            }
            y = yneu;
        }
        return y;
    }
    /*
        Teacher's solution
    */
    public static double wurzelSolution (double a) {
        double xNeu = a;
        double xAlt = -1.0;

        while (Math.abs(xNeu -xAlt) > 1e-8) {
            xAlt = xNeu;
            xNeu = (xAlt +a/xAlt) / 2.0;
            System.out.println("----> " + xNeu);

        }
        return xNeu;
    }
}
