package test;

import java.util.Arrays;

public class SiebDesEratosthenes {
    public static void main(String args[]) {
        /*
            Sieb initialisieren
                1. Setze jedes Array-Element auf TRUE
                2. Setze das nullte und das erste Element auf FALSE
         */
        int obergrenze = 200;
        boolean sieb[] = new boolean[obergrenze + 1];
        Arrays.fill(sieb, Boolean.TRUE);
        sieb[0] = false;
        sieb[1] = false;

        // Schrittweite initialisieren
        int schrittweite = 2;
        // Elemente FALSE setzen
        while (schrittweite <= (obergrenze-schrittweite)) {
            // Indexposition initialisieren
            for (int indexposition = schrittweite+schrittweite; indexposition < obergrenze; indexposition += schrittweite){
                sieb[indexposition] = false;
            }
            // Nächste Schrittweite bestimmen
            do {
                schrittweite++;
            } while(!sieb[schrittweite] && (schrittweite<obergrenze));
        }

        System.out.println("Primzalen: ");
        for (int index = 0; index < sieb.length; index++) {
            if (sieb[index] == true) {
                System.out.println(index);
            }
        }


    }
}

/*

 */