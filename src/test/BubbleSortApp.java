package test;

// BubbleSortApp.java
////////////////////////////////////////////////////////////////
class ArrayBub {
    private long[] a;                 // Array-Referenz
    private int nElems;               // Anzahl Elemente

    //--------------------------------------------------------------
    public ArrayBub(int max)          // Konstruktor
    {
        a = new long[max];                 // Erzeuge Array
        nElems = 0;                        // Noch keine Elemente
    }

    //--------------------------------------------------------------
    public void insert(long value)    // Element in Array einfügen
    {
        a[nElems] = value;             // Einfügen
        nElems++;                      // Grِsse Inkrementieren
    }

    //--------------------------------------------------------------
    public void display()             // Zeige Array
    {
        for (int j = 0; j < nElems; j++)       // Für jedes Elemet
            System.out.print(a[j] + " ");  // Zeige es
        System.out.println("");
    }

    //--------------------------------------------------------------
    public void BubbleSortApp() {
        for (int out = nElems - 1; out > 1; out--) {
            for (int in = 0; in < out; in++) {
                if (a[in] > a[in + 1]) {
                    swap(in, in+1);
                }
            }
        }
    }  // end BubbleSortApp()

    //--------------------------------------------------------------
    private void swap(int one, int two) {
        long tmp1 = a[one];
        a[one] = a[two];
        a[two] = tmp1;
    }
//--------------------------------------------------------------
}  // end class ArrayBub

////////////////////////////////////////////////////////////////
class BubbleSortApp {
    public static void main(String[] args) {
        int maxSize = 100;            // Array-Grِsse
        ArrayBub arr;                 // Array-Referenz
        arr = new ArrayBub(maxSize);  // Erzeuge Array

        arr.insert(77);               // Füge 10 Elemente ein
        arr.insert(99);
        arr.insert(44);
        arr.insert(55);
        arr.insert(22);
        arr.insert(88);
        arr.insert(11);
        arr.insert(00);
        arr.insert(66);
        arr.insert(33);

        arr.display();                // Zeige Elemente

        arr.BubbleSortApp();             // Bubble Sort

        arr.display();                // Zeige Elemente
    }  // end main()
}  // end class BubbleSortApp
////////////////////////////////////////////////////////////////
