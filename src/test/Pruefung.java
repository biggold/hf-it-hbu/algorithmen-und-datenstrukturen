package test;

public class Pruefung {
    /**
     * Zeilenweise Ausgabe des kleinen 1x1.
     */
    public static void main(String[] args) {
        /**
        System.out.println("1 x 1 Tabelle");
        System.out.println("-------------");
        System.out.println();
        // Zeile
        for (int zeile = 1; zeile <= 10; zeile++) {
            // Spalte in der Zeile
            for (int spalte = 1; spalte <= 10; spalte++) {
                // Abstand
                if (zeile * spalte < 10) System.out.print(" ");
                if (zeile * spalte < 100) System.out.print(" ");

                System.out.print("  " + zeile * spalte);  // Produkt
            }

            System.out.println(); // Zeilenumbruch
        }
        System.out.println();
        System.out.println("Ende des Programms");
         **/
        System.out.printf("%.4f",sinTaylor(360, 3));
    }

    public static double sinTaylor(double x, int n) {
        double denom;
        double sum = 0;


        int sign = 1;
        for(int counter = 0; counter < n; counter++){
            denom = counter * 2 + 1;
            double value = Math.pow(x, denom);
            sum += sign * Math.pow(x,denom)/factorial(denom);
            sign *= -1;
        }

        System.out.println();
        return sum;
    }
    public static double factorial(double num){
        if(num == 0) return 1;
        return num * factorial(num -1);
    }
}
