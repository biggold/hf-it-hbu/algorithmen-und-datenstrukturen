package test;

public class Fakultaet {
    public static void main(String[] args) {
        System.out.println(fakultRec(5));
        System.out.println(fakultIter(5));
    }
    public static int fakultIter(int n) {
        int f = 1;
        for(int i = 1; i<=n; i++)
        {
            f *= i;
        }
        return f;
    }
    public static int fakultRec(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return fakultRec(n - 1) * n;
        }
    }
}
