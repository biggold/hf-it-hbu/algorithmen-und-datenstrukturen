Sieb des Eratosthenes

Sieb initialisieren
1. Setze jedes Array-Element auf TRUE
2. Setze das nullte und das erste Element auf FALSE

Schrittweite initialisieren
3. Setze die Schrittweite auf 2

4. Repetiere solange die Schrittweite kleiner als die halbe Array-Länge ist

	Indexposition initialisieren
	4.1 Setze die Indexposition gleich der Schrittweite

	Elemente FALSE setzen
	4.2 Repetiere solange die Indexposition kleiner als die (Arraylänge-Schrittweite) ist
		4.2.1 Inkrementiere die Indexposition um die Schrittweite
   		4.2.2 Setze das indizierte Array-Element auf FALSE

	Nächste Schrittweite bestimmen
	4.3. Repetiere solange das Array-Element an der Indexposition Schrittweite+1 FLASE ist
		4.3.1. Inkrementiere die Schrittposition um eins

Ausgabe des Siebes
9. Gib alle Indexpositionen aus, die auf ein Array-Element zeigen, das TRUE ist


