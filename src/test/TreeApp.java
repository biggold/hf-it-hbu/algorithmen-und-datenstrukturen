package test;

// TreeApp.java

import java.io.*;
import java.util.*;               // F�r Klasse Stack

////////////////////////////////////////////////////////////////
class Node {
    public int iData;              // Datensatz (Schl�ssel)
    public double dData;           // Datensatz
    public Node leftChild;         // Linker Nachfolger
    public Node rightChild;        // Rechter Nachfolger

    public void displayNode()      // Zeige Datens�tze
    {                           // des Knotens
        System.out.print('{');
        System.out.print(iData);
        System.out.print(", ");
        System.out.print(dData);
        System.out.print("} ");
    }
}  // end class Node

////////////////////////////////////////////////////////////////
class Tree {
    private Node root;             // Erster Knoten im Baum (Wurzel)

    // -------------------------------------------------------------
    public Tree()                  // Konstruktor
    {
        root = null;
    }            // Noch keine Knoten im Baum

    // -------------------------------------------------------------
    public int minimum()
    {
        Node current = root;
        while (current.leftChild != null) {
            current = current.leftChild;
        }
        return current.iData;
    }
// -------------------------------------------------------------

    public Node find(int key)      // Finde Knoten mit gesuchtem Schl�ssel
    {                           // (Annahme: Baum nicht leer)
        Node current = root;               // Starte bei der Wurzel
        while (current.iData != key)        // Solange keine �bereinstimmung,
        {
            if (key < current.iData)         // Gehe links?
                current = current.leftChild;
            else                            // Oder gehe rechts?
                current = current.rightChild;
            if (current == null)             // Falls kein Nachfolger,
                return null;                 // dann wurde Knoten nicht gefunden
        }
        return current;                    // Referenz auf gefundenen Knoten
    }  // end find()

    // -------------------------------------------------------------
    public void insert(int id, double dd) {
        Node newNode = new Node();    // Erzeuge neuen Knoten
        newNode.iData = id;           // F�ge Datens�tze ein
        newNode.dData = dd;
        if (root == null)                // Kein Wurzelknoten vorhanden
            root = newNode;
        else                          // Wurzelknoten vorhanden
        {
            Node current = root;       // Starte bei der Wurzel
            Node parent;
            while (true)                // (exits internally)
            {
                parent = current;
                if (id < current.iData)  // Gehe nach links?
                {
                    current = current.leftChild;
                    if (current == null)  // Falls am Ende,
                    {                 // F�ge links ein
                        parent.leftChild = newNode;
                        return;
                    }
                }  // end links
                else                    // Oder gehe nach rechts?
                {
                    current = current.rightChild;
                    if (current == null)  // Falls am Ende,
                    {                 // F�ge rechts ein
                        parent.rightChild = newNode;
                        return;
                    }
                }  // end rechts
            }  // end while
        }  // end else not root
    }  // end insert()

    // -------------------------------------------------------------
    public boolean delete(int key) // L�sche Knoten mit gegebenem Schl�ssel
    {                           // (Annahme: Baum nicht leer)
        Node current = root;
        Node parent = root;
        boolean isLeftChild = true;

        while (current.iData != key)        // Suche den Knoten
        {
            parent = current;
            if (key < current.iData)         // Gehe nach links?
            {
                isLeftChild = true;
                current = current.leftChild;
            } else                            // Oder gehe nach rechts?
            {
                isLeftChild = false;
                current = current.rightChild;
            }
            if (current == null)             // Falls Ende erreicht,
                return false;                // konnte Knoten nicht gefunden werden
        }  // end while

        // Zu l�schender Knoten gefunden

        // Falls Knoten keine Nachfolger hat, l�sche es einfach
        if (current.leftChild == null && current.rightChild == null) {
            if (current == root)             // Falls Wurzelknoten,
                root = null;                 // Wurzel wird leer
            else if (isLeftChild)
                parent.leftChild = null;     // Sonst Knoten
            else                            // vom Vorg�nger abkoppeln
                parent.rightChild = null;
        }

        // Falls kein Nachfolger rechts, ersetze Knoten mit linkem Teilbaum
        else if (current.rightChild == null)
            if (current == root)
                root = current.leftChild;
            else if (isLeftChild)
                parent.leftChild = current.leftChild;
            else
                parent.rightChild = current.leftChild;

            // Falls kein Nachfolger links, ersetze Knoten mit rechtem Tailbaum
        else if (current.leftChild == null)
            if (current == root)
                root = current.rightChild;
            else if (isLeftChild)
                parent.leftChild = current.rightChild;
            else
                parent.rightChild = current.rightChild;

            // Zwei Nachfolger vorhanden, ersetze Knoten mit inorder successor
        else {
            // Finde inorder successor des zu l�schenden Knotens (current)
            Node successor = getSuccessor(current);

            // Verkn�pfe Vorg�nger des zu l�schenden Knotens (current) mit successor
            if (current == root)
                root = successor;
            else if (isLeftChild)
                parent.leftChild = successor;
            else
                parent.rightChild = successor;

            // Verkn�pfe successor mit dem linken Nachfolger von current
            successor.leftChild = current.leftChild;
        }  //
        // (successor kann keinen linken Nachfolger haben)
        return true;                                // Erfolgreich gel�scht
    }  // end delete()

    // -------------------------------------------------------------
    // Gibt Knoten mit n�chstgr�sserem Schl�ssel zu delNode zur�ck
    // Geht zum rechten Nachfolger und dann zu allen linken Nachfolgern
    private Node getSuccessor(Node delNode) {
        Node successorParent = delNode;
        Node successor = delNode;
        Node current = delNode.rightChild;   // Gehe zum rechten Nachfolger
        while (current != null)               // Solange, bis keine
        {                                 // linken Nachfolger mehr,
            successorParent = successor;
            successor = current;
            current = current.leftChild;      // gehe zum linken Nachfolger
        }
        // Falls successor nicht
        if (successor != delNode.rightChild)  // der rechte Nachfolger ist,
        {                                 // Mache Verkn�pfungen
            successorParent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }
        return successor;
    }

    // -------------------------------------------------------------
    public void traverse(int traverseType) {
        switch (traverseType) {
            case 1:
                System.out.print("\nPreorder Traversierung: ");
                preOrder(root);
                break;
            case 2:
                System.out.print("\nInorder Traversierung:  ");
                inOrder(root);
                break;
            case 3:
                System.out.print("\nPostorder Traversierung: ");
                postOrder(root);
                break;
        }
        System.out.println();
    }

    // -------------------------------------------------------------
    private void preOrder(Node localRoot) {
        if (localRoot != null) {
            System.out.print(localRoot.iData + " ");
            preOrder(localRoot.leftChild);
            preOrder(localRoot.rightChild);
        }
    }

    // -------------------------------------------------------------
    private void inOrder(Node localRoot) {
        if (localRoot != null) {
            inOrder(localRoot.leftChild);
            System.out.print(localRoot.iData + " ");
            inOrder(localRoot.rightChild);
        }
    }

    // -------------------------------------------------------------
    private void postOrder(Node localRoot) {
        if (localRoot != null) {
            postOrder(localRoot.leftChild);
            postOrder(localRoot.rightChild);
            System.out.print(localRoot.iData + " ");
        }
    }

    // -------------------------------------------------------------
    public void displayTree() {
        Stack globalStack = new Stack();
        globalStack.push(root);
        int nBlanks = 32;
        boolean isRowEmpty = false;
        System.out.println(
                "......................................................");
        while (isRowEmpty == false) {
            Stack localStack = new Stack();
            isRowEmpty = true;

            for (int j = 0; j < nBlanks; j++)
                System.out.print(' ');

            while (globalStack.isEmpty() == false) {
                Node temp = (Node) globalStack.pop();
                if (temp != null) {
                    System.out.print(temp.iData);
                    localStack.push(temp.leftChild);
                    localStack.push(temp.rightChild);

                    if (temp.leftChild != null ||
                            temp.rightChild != null)
                        isRowEmpty = false;
                } else {
                    System.out.print("--");
                    localStack.push(null);
                    localStack.push(null);
                }
                for (int j = 0; j < nBlanks * 2 - 2; j++)
                    System.out.print(' ');
            }  // end while globalStack not empty
            System.out.println();
            nBlanks /= 2;
            while (localStack.isEmpty() == false)
                globalStack.push(localStack.pop());
        }  // end while isRowEmpty is false
        System.out.println(
                "......................................................");
    }  // end displayTree()
// -------------------------------------------------------------
}  // end class Tree

////////////////////////////////////////////////////////////////
class TreeApp {
    public static void main(String[] args) throws IOException {
        int value;
        Tree theTree = new Tree();

        theTree.insert(50, 1.5);
        theTree.insert(25, 1.2);
        theTree.insert(75, 1.7);
        theTree.insert(12, 1.5);
        theTree.insert(37, 1.2);
        theTree.insert(43, 1.7);
        theTree.insert(30, 1.5);
        theTree.insert(33, 1.2);
        theTree.insert(87, 1.7);
        theTree.insert(93, 1.5);
        theTree.insert(97, 1.5);

        while (true) {
            System.out.print("Eingabe von (s)how, ");
            System.out.print("(i)nsert, (f)ind, (d)elete, oder (t)raverse, (m)inimum: ");
            int choice = getChar();
            switch (choice) {
                case 'm':
                    System.out.println(theTree.minimum());
                    break;
                case 's':
                    theTree.displayTree();
                    break;
                case 'i':
                    System.out.print("Einzuf�gender Wert: ");
                    value = getInt();
                    theTree.insert(value, value + 0.9);
                    break;
                case 'f':
                    System.out.print("Zu suchender Wert: ");
                    value = getInt();
                    Node found = theTree.find(value);
                    if (found != null) {
                        System.out.print("Gefunden: ");
                        found.displayNode();
                        System.out.print("\n");
                    } else
                        System.out.print("Nicht gefunden ");
                    System.out.print(value + '\n');
                    break;
                case 'd':
                    System.out.print("Zu l�schender Wert: ");
                    value = getInt();
                    boolean didDelete = theTree.delete(value);
                    if (didDelete)
                        System.out.print("Gel�scht " + value + '\n');
                    else
                        System.out.print("Konnte nicht l�schen ");
                    System.out.print(value + '\n');
                    break;
                case 't':
                    System.out.print("Traversierungstyp (1) preorder, (2) inorder or (3) postorder: ");
                    value = getInt();
                    theTree.traverse(value);
                    break;
                default:
                    System.out.print("Ung�ltige Eingabe\n");
            }  // end switch
        }  // end while
    }  // end main()

    // -------------------------------------------------------------
    public static String getString() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        return s;
    }

    // -------------------------------------------------------------
    public static char getChar() throws IOException {
        String s = getString();
        return s.charAt(0);
    }

    //-------------------------------------------------------------
    public static int getInt() throws IOException {
        String s = getString();
        return Integer.parseInt(s);
    }
// -------------------------------------------------------------
}  // end class TreeApp
////////////////////////////////////////////////////////////////
