package test;

public class Knuth {
    public static void main(String[] args) {
        System.out.println(gapRec(10));
        System.out.println(gap(10));
    }

    public static int gapRec(int n) {
        if (n / 3 >= 1) {
            return 3 * (n - 1) + 1;
        } else {
            return 1;
        }
    }

    public static int gap(int n) {
        int h = 1;
        while (h <= n / 3) {
            h = 3 * h + 1; // h is equal to highest sequence of h<=length/3
            // (1,4,13,40...)
        }
        return h;
    }
}
